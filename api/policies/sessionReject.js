/**
 * Reject authenticated session
 */

module.exports = function (req, res, next) {
  if (req.session.authenticated) {
    // ASSUMPTION: only admin can login
    return res.redirect('/admin');
  } else {
    return next();
  }
};
