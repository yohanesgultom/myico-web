/**
 * Cache.js
 *
 * @description :: Simple key value collections to store results of expensive calculation
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    key: {
      type: 'string',
      unique: true,
      required: true
    },
    value: {
      type: 'json'
    }

  },

};
