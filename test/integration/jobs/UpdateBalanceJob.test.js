const chai = require('chai'),
    nock = require('nock'),
    should = chai.should(),
    expect = chai.expect,
    fs = require('fs'),
    path = require('path')
    cheerio = require('cheerio');

const emailFile = path.resolve(__dirname, '..', '..', '..', '.tmp', 'email.txt');
const job = require('../../../api/jobs/UpdateBalanceJob')({});

describe('UpdateBalanceJob', function () {

    before(function (done) {
        // delete existing investors
        User
            .destroy({role: User.roles.investor})
            .then(function() { done(); })
            .catch(done);
    })

    describe('#run', function () {

        let dummyInvestor = {
            email: 'updatebalancejob1@gmail.com',
            wallet: 'Aab865311908081b743b8d3ff075DF40c12aeBdF',
            password: 'Aab865311908081b743b8d3ff075DF40c12aeBdF',
            verified: true,
            deposits: {
                bitcoin: {
                    wallet: 'n4FyuAzjSKgAXgFP7zViarWDtu8TXJCrTZ',
                    privateKey: 'cTFHKfjan9xBmL1SH7eeEo3zJgjRkrL7WhWuR5bVoVTLWjADi6JY',
                },
                ether: {
                    wallet: '527ef560fca4f675fb1559a74de8307786fca7a9',
                    privateKey: '7f7ad73b3b06f0494e97615d83c5e2f09fc787836c1c2c798890dee260e41825',
                },
                bitcoinCash: {
                    wallet: 'muWmyGWR2z5Nb18m5ui7wrVgc84UsvLBNH',
                    privateKey: 'cPS2Yq3m1Nc6d3JzFy5hZZrp8u67crbD9rHwCcVrYt3uNuGadZLv',
                },
                dash: {
                    wallet: 'yP72P9rZYggDWT9tJYUVHFNhbR1GjeDwnF',
                    privateKey: 'cVY3VJ63D4BYnRjh7Pn4xZaLtVbseQ5AD35g48UtL34b3mqtuM3o',
                },
                litecoin: {
                    wallet: 'LQZbqytqhrHH8dREXrov9iVg9J3SSMxyuC',
                    privateKey: 'T4hzKKBWu2PPLXfoBaWvjR94F6QXxr3G5SNnQLK2Lsa4zHDBMtr8',
                }
            }
        };

        // urls to be intercepted
        let wallets = {
            ether: {
                address: dummyInvestor.deposits.ether.wallet,
                balance: '2453021500000000000',
                expected: '2.4530215',
            },
            bitcoin: {
                address: dummyInvestor.deposits.bitcoin.wallet,
                balance: '65990000',
                expected: '0.6599',
            },
            bitcoinCash: {
                address: dummyInvestor.deposits.bitcoinCash.wallet,
                balance: '65990000',
                expected: '0.6599',
            },
            dash: {
                address: dummyInvestor.deposits.dash.wallet,
                balance: '6.9989548',
                expected: '6.9989548',
            },
            litecoin: {
                address: dummyInvestor.deposits.litecoin.wallet,
                balance: '0.4618',
                expected: '0.4618',
            },
        };

        before(function (done) {
            // create investor
            User.create(dummyInvestor)
                .then(function (user) {
                    investor = user;
                    // delete email file
                    fs.unlink(emailFile, function () { done() });
                })
                .catch(done);
            
        });

        it('should send notification email on new transactions', function (done) {

            this.timeout(10000);

            // ether
            nock(sails.config.thirdParty.etherscan.url)
                .get('')
                .query({
                    module: 'account',
                    action: 'balance',
                    tag: 'latest',
                    address: '0x' + wallets.ether.address,
                    apiKey: sails.config.thirdParty.etherscan.apiKey,
                })
                .reply(200, {
                    status: '1',
                    message: 'OK',
                    result: wallets.ether.balance,
                });

            // bitcoin
            nock(sails.config.thirdParty.blockCypher.url.bitcoin)
                .get('/addrs/' + wallets.bitcoin.address + '/balance')
                .reply(200, {
                    balance: wallets.bitcoin.balance,
                });

            // bitcoinCash
            nock(sails.config.thirdParty.blockTrail.url['bitcoinCash'])
                .get('/address/' + wallets.bitcoinCash.address)
                .query({
                    api_key: sails.config.thirdParty.blockTrail.apiKey,
                })
                .reply(200, {
                    balance: wallets.bitcoinCash.balance,
                });

            // dash
            nock(sails.config.thirdParty.soChain.url)
                .get('/get_address_balance/' + sails.config.thirdParty.soChain.network['dash'] + '/' + wallets.dash.address)
                .reply(200, {
                    data: {
                        confirmed_balance: wallets.dash.balance,
                    }
                });
                
            // litecoin
            nock(sails.config.thirdParty.soChain.url)
                .get('/get_address_balance/' + sails.config.thirdParty.soChain.network['litecoin'] + '/' + wallets.litecoin.address)
                .reply(200, {
                    data: {
                        confirmed_balance: wallets.litecoin.balance,
                    }
                });

            // run job
            job.run({ attrs: { name: 'UpdateBalanceJobTest' } }, function (err) {
                expect(err).to.be.undefined;
                
                // wait for sendEmail job
                setTimeout(function () {
                    fs.readFile(emailFile, 'utf-8', function (err, data) {
                        if (err) done(err);
                        // get the latest email
                        let lines = data.trim().split('\n');
                        let email = JSON.parse(lines[lines.length - 1]);
                        email.to.should.equals(sails.config.email.from);
                        email.subject.should.equals('New transaction(s)');
                        let $ = cheerio.load(email.html);
                        // subjected to email template
                        $('li').each(function (i, elem) {
                            let values = $(this).text().split(' ');
                            values[0].should.equal(dummyInvestor.email);
                            values[1].should.equal(wallets[values[2]].expected)
                        });
                        done();
                    });
                }, 3000);
            });
        });


        it('should NOT send notification email if no new transaction', function (done) {

            this.timeout(10000);

            // ether
            nock(sails.config.thirdParty.etherscan.url)
                .get('')
                .query({
                    module: 'account',
                    action: 'balance',
                    tag: 'latest',
                    address: '0x' + wallets.ether.address,
                    apiKey: sails.config.thirdParty.etherscan.apiKey,
                })
                .reply(200, {
                    status: '1',
                    message: 'OK',
                    result: wallets.ether.balance,
                });

            // bitcoin
            nock(sails.config.thirdParty.blockCypher.url.bitcoin)
                .get('/addrs/' + wallets.bitcoin.address + '/balance')
                .reply(200, {
                    balance: wallets.bitcoin.balance,
                });

            // bitcoinCash
            nock(sails.config.thirdParty.blockTrail.url['bitcoinCash'])
                .get('/address/' + wallets.bitcoinCash.address)
                .query({
                    api_key: sails.config.thirdParty.blockTrail.apiKey,
                })
                .reply(200, {
                    balance: wallets.bitcoinCash.balance,
                });

            // dash
            nock(sails.config.thirdParty.soChain.url)
                .get('/get_address_balance/' + sails.config.thirdParty.soChain.network['dash'] + '/' + wallets.dash.address)
                .reply(200, {
                    data: {
                        confirmed_balance: wallets.dash.balance,
                    }
                });

            // litecoin
            nock(sails.config.thirdParty.soChain.url)
                .get('/get_address_balance/' + sails.config.thirdParty.soChain.network['litecoin'] + '/' + wallets.litecoin.address)
                .reply(200, {
                    data: {
                        confirmed_balance: wallets.litecoin.balance,
                    }
                });

            // make sure file is deleted
            fs.unlink(emailFile, function (err) { 

                // run job again            
                job.run({ attrs: { name: 'UpdateBalanceJobTest' } }, function (err) {
                    expect(err).to.be.undefined;

                    // wait for sendEmail job and make sure not file created
                    setTimeout(function () {
                        fs.readFile(emailFile, 'utf-8', function (err, data) {
                            if (err) {
                                done();
                            } else {
                                // let lines = data.trim().split('\n');
                                // let email = JSON.parse(lines[lines.length - 1]);
                                // console.log(email.html);
                                done('Email file should not exist');
                            }
                        });
                    }, 3000);
                });

            });

        });

    });


});
