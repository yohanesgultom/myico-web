$(function(){
   $('#sample_goal').goalProgress({
		goalAmount: 8000,
		currentAmount: 2410,
		textBefore: '',
		textAfter: 'BTC'
	});

   // First doughnut Chart
   var first = $('#first').get(0);
   var chart = new Chart(first, {
       // The type of chart we want to create
       type: 'doughnut',

       // The data for our dataset
       data: {
           labels: ["INFRASTRUCTURE","INDIRECT","MARKETING","TEAM","R&D",] ,
           datasets: [{
               label: 'First doughnut',
               data: ["5","5","55","20","15",],
               backgroundColor: [ 'rgb(251,95,85)', 'rgb(127,170,136)', 'rgb(46,192,249)','rgb(3,29,68)','rgb(255,210,117)'],
               borderColor: 'rgba(127,170,136,0)',
           }]
       },
       // Configuration options go here
       options: {
           title: {
               display: true,
               text: 'Token distribution structure',
               fontSize: 23,
               fontColor: '#ffffff',
               fontFamily: "'Montserrat', sans-serif",
               fontStyle: 'lighter',
               padding: 30
           },
           legend: {
               display: false
           }
       }
   });


// Bar Chart
   var bar = $('#bar').get(0);
   var chart = new Chart(bar, {
       // The type of chart we want to create
       type: 'bar',

       // The data for our dataset
       data: {
           labels: ["LTC","ETH","DASH","BTC","BCH","ZEC"],
           datasets: [{
               data: ["0.15","0.3","0.16","0.4","0.15","0.14"],
               backgroundColor: [
                   'rgb(244,91,95)',
                   'rgb(233,162,215)',
                   'rgb(220,233,162)',
                   'rgb(102,106,101)',
                   'rgb(162,229,233)',
                   'rgb(127,170,136)'
               ],
               borderColor: 'rgba(127,170,136,0)',
           }]
       },
       // Configuration options go here
       options: {
           title: {
               display: true,
               text: 'Cryptocurrency Invested',
               fontSize: 23,
               fontColor: '#ffffff',
               position: 'bottom',
               fontFamily: "'Montserrat', sans-serif",
               fontStyle: 'lighter'
           },
           legend: {
               display: false
           },
           scales: {
               xAxes: [{
                   gridLines: {
                       drawOnChartArea: false
                   }
               }],
               yAxes: [{
                   gridLines: {
                       drawOnChartArea: false
                   }
               }]
           }
       }
   });

});