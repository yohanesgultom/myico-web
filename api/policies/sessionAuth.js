/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function(req, res, next) {
  let err = null;
  if (!req.session.authenticated) err = 'Please login to access the page';
  if (err) {
    req.session.authenticated = false;
    req.session.user = null;
    req.session.flash = { err: err };
    return res.redirect('/auth/login');
  } else {
    // ASSUMPTION: only admin can login
    // currently investor can't login
    res.locals.layout = 'admin';
    return next();
  }
};
