// resend.js


$(document).ready(function() {

  /**
   * Submit resend email form
   */
  $('#resend-btn').on('click', function () {
    let inputEmail = $('input[name=email]', '.resend-form');
    let email = inputEmail.val();
    if (email) {
      let data = { email: email };
      postSignUpData(data, $(this));
      inputEmail.val('');
    }
    return false;
  });
  
})


/**
 * Submit resend email data to server
 * @param {*} data 
 * @param {*} form 
 * @param {*} btn 
 */
function postSignUpData(data, btn) {
  btn.prop('disabled', true);

  // submit valid form    
  $.post('/auth/resend', data, function (res) {
    hm_openDialog('Link to your page has been re-sent to your email.', 'ok')
  })
  .fail(function (err) {
    if (err.status == '404') {
      hm_openDialog('Email not found', 'error');
    } else {
      console.error(err);
      hm_openDialog(err.responseText || 'Server error. Please contact administrator', 'error');
    }
    btn.prop('disabled', false);
  });
}