// join.js

const validation_status = {
  email:false,
  wallet:false
};


$(document).ready(function() {

  /**
   * Select crypto currencies
   */
  $('.hm-block-item').on('click', function () {
    let el = $(this);
    let selected = 'active';
    if (!el.hasClass(selected)) {
      el.addClass(selected);
    } else {
      el.removeClass(selected);
    }
  });


  /**
   * Submit join form
   */
  let form = $('#join-form');
  $('.btn[type=submit]', form).on('click', function () {

    // Term condition validation
    if (!$('input[name=agreeWithTC')[0].checked){
      hm_openDialog('Please Check  I Agree with Terms & Coditions  ','error');
      return false;
    }

    if(!validation_status.email){
      hm_openDialog('Invalid email Address !','error');
      return false;
    }

    if(!validation_status.wallet){
      hm_openDialog('Invalid Ethereum Wallet !','error');
      return false;
    }

    // validation leveraging HTML5 validation
    if (!form[0].checkValidity()) return true;

    let data = serializeForm(form);

    // selected deposits
    data.currencies = [];
    $('.selectable.active', '#currency-selections').each(function () {
      data.currencies.push($(this).attr('data-currency'));
    });

    // currencies validation
    if (data.currencies.length == 0) {
      hm_openDialog('Please select currencies','error');
      return false;
    }

    if (data.nonUSCitizenDeclaration) {
      // post data to server
      postSignUpData(data, form, $(this));
    } else {
      // check country code
      $.getJSON('/page/checkCountry', function (res) {
        if (res.country && res.country.toUpperCase() == 'US') {
          // prompt for declaration
          hm_openNonUSCitizenDeclarationDialog();
        } else {
          // post data to server
          postSignUpData(data, form, $(this));
        }
      })
      .fail(function () {
        hm_openDialog('Please check your connection and try again or contact administrator', 'error');
        return false;
      });
    }

    return false;
  });

  /**
   * On blur email and wallet validations
   */
  $("input[name=email]").on('blur keyup change', function() { email_validation(this); });
  $("input[name=wallet]").on('blur keyup change',function(){ ethwallet_validation(this); });

})


/**
 * Submit signup data to server
 * @param {*} data
 * @param {*} form
 * @param {*} btn
 */
function postSignUpData(data, form, btn) {
  btn.prop('disabled', true);

  // submit valid form
  $.post('/auth/signup', data, function (res) {
    hm_openDialog('Thanks for signing up. We are sending further instruction to your email address.', 'ok')
    form[0].reset();
  })
  .fail(function (err) {
    hm_openDialog(err.responseText || 'There seems to be a problem with our server. Please try again or contact our support', 'error');
    console.error(err);
    btn.prop('disabled', false);
  });
}


function email_validation(src){
  var email_txt= $(src).val();
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
  var elem = $(src).next('#msg');
  elem.hide();
  if (re.test(email_txt)) {
    elem.html('Valid Email Address');
    elem.attr('style','color:green').fadeIn();
    validation_status.email=true;
  } else {
    elem.html('Invalid Email Address');
    elem.attr('style','color:red').fadeIn();
    validation_status.email=false;
  }
}


function ethwallet_validation(src){
  var wallet = $(src).val(),
  isValid=false,
  elem = $(src).next('#msg');
  if(typeof web3 != 'undefined'){
    isValid = web3.isAddress(wallet);
    elem.hide();
    if (isValid) {
      elem.html('Valid Wallet');
      elem.attr('style','color:green').fadeIn();
      validation_status.wallet=true;
    } else {
      elem.html('Invalid wallet');
      elem.attr('style','color:red').fadeIn();
      validation_status.wallet=false;
    }
  }else{
    $.get('/page/checkEthAddr?wallet='+wallet, function (res) {
      isValid = res.status=='valid'?true:false;
      elem.hide();
      if (isValid) {
        elem.html('Valid Wallet');
        elem.attr('style','color:green').fadeIn();
        validation_status.wallet=true;
      } else {
        elem.html('Invalid wallet');
        elem.attr('style','color:red').fadeIn();
        validation_status.wallet=false;
      }
    })
    .fail(function (err) {
      console.log(err.responseText);
    });
  }
}

/**
 * Open declaration form
 */
function hm_openNonUSCitizenDeclarationDialog() {
  $('.hm-dialog-layer').remove();
  $('.hm-dialog').remove();
  let msg = 'We detected that you are accessing from US IP address. Please confirm that you are Non-US citizen to proceed';
  let dlg = $('<div class="hm-dialog-layer"></div><div class="anim hm-dialog hm-block-item hm-big-box dialog-info"><div class="hm-dialog-bg"></div><div class="hm-dialog-icon"></div><div class="hm-dialog-body"><p>' + msg + '</p></div><div class="hm-dialog-footer"><div class="text-left"><a href="#" class="btn hm-btn-green" onclick="declareNonUSCitizen();return false;">I am a Non-US citizen</a><a href="#" class="btn hm-btn-red" onclick="hm_closeNonUSCitizenDeclarationDialog();return false;">Cancel</a></div></div></div>');
  $('body').append(dlg);
  $('body').addClass('with-dialog');
}


/**
 * Close and
 */
function hm_closeNonUSCitizenDeclarationDialog() {
  $('body').removeClass('with-dialog');
  $('.hm-dialog-layer').remove();
  $('.hm-dialog').remove();
}


/**
 * Append declaration hidden input and resubmit
 */
function declareNonUSCitizen() {
  let form = $('#join-form');
  let inputName = 'nonUSCitizenDeclaration';
  $('input[name=' + inputName + ']', form).remove();
  let hiddenInput = $('<input>').attr('type', 'hidden').attr('name', inputName).val(true);
  form.prepend(hiddenInput);
  // destroy dialog
  hm_closeNonUSCitizenDeclarationDialog();
  // resubmit
  $('.btn[type=submit]', form).click();
}


