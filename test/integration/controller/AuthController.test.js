const chai = require('chai'),
  chaiHttp = require('chai-http'),
  should = chai.should(),
  fs = require('fs'),
  path = require('path');

const emailFile = path.resolve(__dirname, '..', '..', '..', '.tmp', 'email.txt');

chai.use(chaiHttp);

describe('AuthController', function () {

  describe('#sessionAuth', function () {
    it('should redirect anonymous to /auth/login', function (done) {
      chai
        .request(sails.hooks.http.app)
        .get('/admin')
        .end(function (err, res) {
          res.should.redirectTo(res.request.protocol + '//' + res.request.host + '/auth/login');
          done();
        });
    });
  });

  // TODO: handle Google Recaptcha
  // describe('#login', function () {
  //   it('should redirect authenticated admin to /user', function (done) {
  //     chai
  //       .request(sails.hooks.http.app)
  //       .post('/auth/login')
  //       .send({
  //         email: 'admin@holdme.io',
  //         password: 'admin',
  //       })
  //       .end(function (err, res) {
  //         res.should.redirectTo(res.request.protocol + '//' + res.request.host + '/admin');
  //         done();
  //       });
  //   });
  // });


  // describe('#signup', function () {

  //   it('should be able to signup from non-US country', function (done) {
  //     chai
  //       .request(sails.hooks.http.app)
  //       .post('/auth/signup')
  //       .set('cf-ipcountry', 'ID')
  //       .send({
  //         email: 'test1@gmail.com',
  //         wallet: '0x4717BB2B286C5C1Edfb9572977318A69cbd98f90',
  //         currencies: ['bitcoin', 'ether'],
  //       })
  //       .end(function (err, res) {
  //         res.status.should.equals(200);
  //         done();
  //       });
  //   });

  //   it('should NOT be able to signup from US without declaration', function (done) {
  //     chai
  //       .request(sails.hooks.http.app)
  //       .post('/auth/signup')
  //       .set('cf-ipcountry', 'US')
  //       .send({
  //         email: 'test2@gmail.com',
  //         wallet: '0x32Be343B94f860124dC4fEe278FDCBD38C102D88',
  //         currencies: ['bitcoin', 'ether'],
  //       })
  //       .end(function (err, res) {
  //         res.status.should.equals(500);
  //         res.text.should.equals('Must declare non-US citizen');
  //         done();
  //       });
  //   });

  //   it('should be able to signup from US with declaration', function (done) {
  //     chai
  //       .request(sails.hooks.http.app)
  //       .post('/auth/signup')
  //       .set('cf-ipcountry', 'US')
  //       .send({
  //         email: 'test3@gmail.com',
  //         wallet: '0x6f46cf5569aefa1acc1009290c8e043747172d89',
  //         currencies: ['bitcoin', 'ether'],
  //         nonUSCitizenDeclaration: true,
  //       })
  //       .end(function (err, res) {
  //         res.status.should.equals(200);
  //         done();
  //       });
  //   });

  // });


  describe('#resend', function () {

    let testUser = {
      email: 'test4@email.com',
      wallet: '46467CA9FfE3E8A5eB9BfB8f50797A324BB93573',
      password: '46467CA9FfE3E8A5eB9BfB8f50797A324BB93573',
      deposits: {},
    }

    before(function (done) {
      // create test user
      User
        .create(testUser)
        .then(function (user) {
          testUser.id = user.id;
          done();
        })
        .catch(done);
    });

    it('should resend email containing investor wallet URL', function (done) {
      this.timeout(10000);
      // delete old emails first
      fs.unlink(emailFile, function () { 
        chai
          .request(sails.hooks.http.app)
          .post('/auth/resend')
          .send({
            email: testUser.email,
          })
          .end(function (err, res) {
            res.status.should.equals(200);
            // wait for send mail job
            setTimeout(function () {
              fs.readFile(emailFile, 'utf-8', function (err, data) {
                if (err) throw err;
                // get the latest email
                let lines = data.trim().split('\n');                
                let email = {}
                for (let i = 0; i < lines.length; i++) {
                  email = JSON.parse(lines[i]);
                  if (email.to == testUser.email) {
                    break;
                  }
                }
                email.to.should.equals(testUser.email);
                email.subject.should.equals('Your Investor Page');
                done();
              });
            }, 4000);
          });
      });

    });

  });


});
