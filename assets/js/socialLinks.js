(function () {
  var elements = $('.social-links');
  var configMap = {
    telegram: {
      class: 'icon telegram',
      img: '/images/icon-{}-telegram.png',
      url: 'https://t.me/{}',
    },
    slack: {
      class: 'icon slack',
      img: '/images/icon-{}-slack.png',
      url: 'https://{}.slack.com',
    },
    reddit: {
      class: 'icon reddit',
      img: '/images/icon-{}-reddit.png',
      url: 'https://www.reddit.com/r/{}/',
    },
    facebook: {
      class: 'icon facebook',
      img: '/images/icon-{}-facebook.png',
      url: 'https://www.facebook.com/{}/',
    },
    twitter: {
      class: 'icon twitter',
      img: '/images/icon-{}-twitter.png',
      url: 'https://twitter.com/{}',
    },
    BTCTalk: {
      class: 'icon bitcoin',
      img: '/images/icon-{}-bitcoin.png',
      url: 'https://bitcointalk.org/{}',
    },
  };

  // make sure it's empty
  elements.empty();

  // load config from server
  $.getJSON('/page/socialMedia')
    .done(function (socialMedia) {
      console.log(JSON.parse(socialMedia));
      for (var name in socialMedia) {
        var config = configMap[name];
        if (config) {
          elements.each(function (i, el) {
            var color = $(el).hasClass('dark') ? 'sb' : 'sw';
            var link = $('<a>')
              .attr('href', config.url.replace('{}', socialMedia[name]))
              .attr('target', '_blank')
              .attr('class', config.class);
            link.append($('<img>').attr('src', config.img.replace('{}', color)));
            $(el).append(link);
            $(el).append(' ');
          });
        }
      }
    })
    .fail(function (err) {
      console.error('Social media accounts not available from server');
    });

})();