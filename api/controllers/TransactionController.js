/**
 * TransactionController
 *
 * @description :: Server-side logic for managing Transactions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var _validateInput = function(paramObj) {
    return User
        .findOne(paramObj.userId)
        .then(function(user) {
            if (!user) throw new Error('Invalid user');
            return Param.findOne({
                key: 'acceptedCurrencies'
            });
        })
        .then(function(param) {
            if (!param) throw new Error('Param not found');
            if (!param.value[paramObj.currency]) throw new Error('Currency is not accepted: ' + paramObj.currency);
            return Promise.resolve();
        });
}


module.exports = {

    'new': function(req, res) {
        let acceptedCurrencies = {};
        Param.findOne({
                key: 'acceptedCurrencies'
            }).then(function(param) {
                acceptedCurrencies = param.value;
                res.view({
                    acceptedCurrencies: acceptedCurrencies
                });
            })
            .catch(function(err) {
                sails.log.error(err.message);
                res.view({
                    acceptedCurrencies: acceptedCurrencies
                })
            });
    },

    index: function(req, res) {
        res.view();
    },

    create: function(req, res) {

        var paramObj = {
            userId: req.param('userId'),
            currency: req.param('currency'),
            value: req.param('value'),
        }

        _validateInput(paramObj)
            .then(function() {
                return Transaction.create(paramObj);
            })
            .then(function(newTransaction) {
                res.redirect('/transaction');
            })
            .catch(function(err) {
                sails.log.error(err);
                req.session.flash = {
                    err: err.message
                };
                return res.redirect('/transaction/new');
            });
    },

    list: function(req, res, next) {
        let start = req.param('start') || 0;
        let limit = req.param('length') || 10;
        let count = 0;
        Transaction
            .count()
            .then(function(res) {
                count = res;
                return Transaction
                    .find()
                    .sort('createdAt DESC')
                    .limit(limit)
                    .skip(start);
            })
            .then(function(transactions) {
                return res.json({
                    draw: req.param('draw'),
                    data: transactions,
                    recordsFiltered: count,
                    recordsTotal: count,
                });
            })
            .catch(function(err) {
                sails.log.error(err);
                if (err) return next(err);
            });
    },


    edit: function(req, res, next) {
        let acceptedCurrencies = {};
        Param.findOne({
                key: 'acceptedCurrencies'
            }).then(function(param) {
                acceptedCurrencies = param.value;
                return Transaction.findOne(req.param('id'));
            })
            .then(function(transaction) {
                if (!transaction) throw new Error('transaction doesn\'t exist.');
                res.view({
                    acceptedCurrencies: acceptedCurrencies,
                    transaction: transaction
                });
            })
            .catch(function(err) {
                sails.log.error(err.message);
                req.session.flash = {
                    err: err.message
                };
                return res.redirect('/transaction/new');
            });
    },

    update: function(req, res, next) {

        var paramObj = {
            userId: req.param('userId'),
            currency: req.param('currency'),
            value: req.param('value'),
        }

        _validateInput(paramObj)
            .then(function() {
                return Transaction.update(req.param('id'), paramObj);
            })
            .then(function(savedTransaction) {
                res.redirect('/transaction');
            })
            .catch(function(err) {
                sails.log.error(err);
                req.session.flash = {
                    err: err.message
                };
                return res.redirect('/transaction/edit/' + req.param('id'));
            });

    },

    destroy: function(req, res, next) {
        Transaction.findOne(req.param('id'), function(err, transaction) {
            if (err) return next(err);
            if (!transaction) return next('Transaction doesn\'t exist.');
            Transaction.destroy(req.param('id'), function(err) {
                if (err) return next(err);
            });
            return res.redirect('/transaction');
        });
    },


    apiTest: function(req, res) {
        if (req.method == 'GET') {
            return res.view();
        } else if (req.method == 'POST') {
            let params = req.params.all();
            if (params.api == 'checkBalance') {
                BlockChainService
                    .checkBalance(params.currency, params.address)
                    .then(function(amount) {
                        return res.json({
                            type: params.currency,
                            address: params.address,
                            balance: amount
                        });
                    })
                    .catch(function(err) {
                        return res.send(500, err);
                    });
            } else if (params.api == 'totalTokenIssued') {
                SmartContractService
                    .totalTokenIssued()
                    .then(function (totalToken) {
                        return res.json({
                            totalToken: totalToken,
                        });
                    })
                    .catch(function (err) {
                        return res.send(500, err);
                    });
            } else if (params.api == 'contributeCoins') {
                SmartContractService
                    .contributeCoins(params.address, params.coins)
                    .then(function (obj) {
                        return res.json(obj);
                    })
                    .catch(function (err) {
                        return res.send(500, err);
                    });
            } else {
                return res.notFound();
            }
        } else {
            return res.notFound();
        }
    },
};