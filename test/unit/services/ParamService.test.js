const chai = require('chai'),
  moment = require('moment'),
  should = chai.should(),
  expect = chai.expect;

describe('ParamService', function () {

  describe('#getAcceptedCurrenciesAndCurentPhase', function () {

    it('should be able to get accepted currencies and current phase', function (done) {
      ParamService
        .getAcceptedCurrenciesAndCurentPhase()
        .then(function (res) {
          res.acceptedCurrencies.should.have.all.keys(['bitcoin', 'ether']);
          res.currentPhase.should.have.all.keys(['name', 'priceETH', 'endDateTime']);
          done();
        })
        .catch(done);      
    });

  });


  describe('#getMultipleParams', function () {

    it('should be able to get acceptedCurrencies, phases and roadmap params in one go', function (done) {
      let keys = ['acceptedCurrencies', 'phases']
      ParamService
        .getMultipleParams(keys)
        .then(function (res) {
          res.should.have.all.keys(keys);
          res.acceptedCurrencies.should.have.all.keys(['bitcoin', 'ether']);
          done();
        })
        .catch(done);
    });

  });


  describe('#getNextPhase', function () {

    let phases = {
      preparation: {
        name: 'Before Pre-Sale',
        priceETH: 0.00013333333333333334,
        endDateTime: '2017-09-30 23:59:59.000+07:00',
      },
      preSale: {
        name: 'Pre-Sale',
        priceETH: 0.00013333333333333334,
        endDateTime: '2017-11-30 23:59:59.000+07:00',
      },
      sale: {
        name: 'Sale',
        priceETH: 0.0003333333333333333,
        endDateTime: '2017-12-31 23:59:59.000+07:00',
      }
    };

    it('should return preparation if date is 2017-09-25 23:59:59.000+07:00', function (done) {
      let phase = ParamService.getNextPhase(phases, moment('2017-09-25 23:59:59.000+07:00'));
      phase.name.should.equals(phases.preparation.name);
      done();
    });

    it('should return preSale if date is 2017-10-01 23:59:59.000+07:00', function (done) {
      let phase = ParamService.getNextPhase(phases, moment('2017-10-01 23:59:59.000+07:00'));
      phase.name.should.equals(phases.preSale.name);
      done();
    });

    it('should return sale if date is 2017-12-01 23:59:59.000+07:00', function (done) {
      let phase = ParamService.getNextPhase(phases, moment('2017-12-01 23:59:59.000+07:00'));
      phase.name.should.equals(phases.sale.name);
      done();
    });

  });

});
