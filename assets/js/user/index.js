
$(document).ready(function () {

    var d = $('#data-table-users').DataTable({
        'searching': false,
        'lengthChange': false,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': '/user/list',
        'columnDefs': [{
            'searchable': false,
            'orderable': false,
            'targets': 0
        }],
        'columns': [
            { 
                'data': null,
                'render': function (data, type, full, meta) {
                    return d.page.info().start + meta.row + 1;
                }
            },
            { 'data': 'id', 'visible': false },
            { 'data': 'email' },
            { 'data': 'role' },
            { 'data': 'wallet' },
            { 
                'data': null, 
                'render': function (data, type, full, meta) {
                    return '<button data-id="' + full.id + '" class="btn pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-success btn-toggle-transfer" role="button"><i class="material-icons">attach_money</i></button>' +
                    '<a href="/user/edit/' + full.id + '" class="btn pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" role="button"><i class="material-icons">create</i></a>' +
                    '<a href="/user/destroy/' + full.id + '" class="btn pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-danger btn-sm" onclick="return confirm(\'Delete this record?\')" role="button"><i class="material-icons">clear</i></a>';
                }
            },
        ]
    });


    // on datatable load
    d.on('draw', function() {

        // bind transfer button click
        $('.btn-toggle-transfer').on('click', function () {
            let btn = $(this);
            let data = d.row(btn.parent().parent()).data();
            $('input[name="userId"]', '#modal-transfer').val(data.id);
            $('input[name="email"]', '#modal-transfer').val(data.email);
            $('input[name="wallet"]', '#modal-transfer').val(data.wallet);
            $('input[name="ether"]', '#modal-transfer').val(0.001);
            $('#modal-transfer').modal({
                keyboard: false
            }).on('shown.bs.modal', function () {
                $('input[autofocus]').focus();
            });
        });

    });


    // load current rate
    $.getJSON('/page/exchangeRates', function (res) {
        $('.exchange-rates').empty();
        let rates = res.acceptedCurrencies;
        let exchangeRateList = [];   
        for (let key in rates) {
            let rate = rates[key];
            exchangeRateList.push(
                $('<div>').attr('class', 'col-md-6')
                    .append($('<div>').attr('class', 'media-left media-body').text('1 ' + rate.symbol + ' = ' + rate.rateToETH.toFixed(4) + ' ETH'))
                    .append($('<div>').attr('class', 'media-middle list-group-item-text').text('Last update: ' + moment(rate.lastUpdatedAt).fromNow()))
            );
        }
        $('.exchange-rates').append(exchangeRateList);
    })
    .fail(function (err) {
        alert('Failed to load exchange rates');
        console.error(err);
    });


    // modal submit button
    $('#btn-transfer').click(function () {

        let btn = $(this);
        let data = {}
        btn.prop('disabled', true);
        $('#modal-transfer form:first-child')
            .serializeArray()
            .map(function (n, i) {                
                data[n.name] = n.value;
            });

        // submit valid form
        $.post('/user/transfer', data, function (res) {
            alert('Success');
            console.log(res);
            // enable submit button
            btn.prop('disabled', false);
            // close modal
            $('#modal-transfer').modal('hide');
            // reload datatable
            d.ajax.reload();
        })
        .fail(function (err) {
            alert(err.responseText || 'There seems to be a problem with our server. Please try again or contact our support');
            console.error(err);
            btn.prop('disabled', false);
        });

        return false;
    });
});
