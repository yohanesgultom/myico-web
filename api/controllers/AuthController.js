const 
  bcrypt = require('bcrypt'),
  request = require('request');

module.exports = {

  login: function(req, res, next) {
    if (req.method == 'GET') {
      if (req.session.authenticated) {
        return res.redirect('/');
      } else {
        return res.view({
          layout: 'public'
        });
      }
    } else if (req.method == 'POST') {
      let params = req.params.all();

      // validate recaptcha
      if (!params['g-recaptcha-response']) {
        req.session.flash = {
          err: 'Invalid recaptcha'
        };
        return res.redirect('/auth/login');
      } 
      
      // verify recaptcha
      new Promise(function (resolve, reject) {
        request.post({
          url: sails.config.recaptcha.url,
          qs: {
            secret: sails.config.recaptcha.secretKey,
            response: params['g-recaptcha-response'],
          },
        },
          function (err, res, body) {
            if (err) {
              reject(err);
            } else {
              body = JSON.parse(body);
              if (body.success != true) {
                reject(new Error('Invalid recaptcha response'));
              } else {
                sails.log.debug(body);
                resolve(body.success);
              }
            }
          }
        );
      })
      .then(function() {
        User.findOne({
          email: req.param('email')
        }, function (err, user) {
          if (err) {
            req.session.flash = {
              err: err.message
            };
            return res.redirect('/auth/login');
          } else if (!user || !User.comparePassword(req.param('password'), user.password)) {
            req.session.flash = {
              err: 'Wrong email and/or password'
            };
            return res.redirect('/auth/login');
          } else {
            // success
            req.session.authenticated = true;
            req.session.user = user;
            if (user.role == User.roles.admin) {
              return res.redirect('/admin');
            } else {
              return res.redirect('/');
            }
          }
        });
      })
      .catch(function(err) {
        req.session.flash = {
          err: err.message
        };
        return res.redirect('/auth/login');
      });
    }
  },


  logout: function(req, res, next) {
    if (req.session.authenticated) {
      req.session.authenticated = false;
      delete req.session.user;
      return res.redirect('/');
    } else {
      return res.redirect('/');
    }
  },


  signup: function(req, res) {
    if (req.method == 'POST') {
      let params = req.params.all();
      let deposits = {}

      // validate recaptcha
      if (!params['g-recaptcha-response']) {
        return res.send(400, 'Invalid recaptcha');
      } 
      
      // remove 0x for consistency
      if (params.wallet.startsWith('0x')) {
        params.wallet = params.wallet.substring(2);
      }

      // validate geolocation country
      // and save them  as additional info
      sails.log.info('country: ' + req.headers['cf-ipcountry']); // must not be US
      sails.log.info('ip: ' + req.headers['cf-connecting-ip']);
      if (req.headers['cf-ipcountry']
        && req.headers['cf-ipcountry'].toUpperCase() == 'US'
        && !params.nonUSCitizenDeclaration) {
        return res.send(400, 'Must declare non-US citizen');
      }

      // defer address generation until email verified
      params.currencies.forEach(function(cur) {
        deposits[cur] = {
          wallet: null,
          privateKey: null,
        }
      });

      let paramObj = {
        email: params.email,
        wallet: params.wallet,
        password: params.wallet,
        deposits: deposits,
        registrationIP: req.headers['cf-connecting-ip'],
        registrationCountry: req.headers['cf-ipcountry'],
        nonUSCitizenDeclaration: params.nonUSCitizenDeclaration == 'true',
      };

      let newUser = null;

      // verify recaptcha
      new Promise(function(resolve, reject) {
        request.post({
          url: sails.config.recaptcha.url,
          qs: {
            secret: sails.config.recaptcha.secretKey,
            response: params['g-recaptcha-response'],
          },
        },
          function (err, res, body) {
            if (err) {              
              reject(err);
            } else {
              body = JSON.parse(body);
              if (body.success != true) {
                reject(new Error('Invalid recaptcha response'));
              } else {
                sails.log.debug(body);
                resolve(body.success);
              }
            }
          }
        );
      })
        .then(User.generateVerificationToken)
        .then(function (token) {
          paramObj.verified = false;
          paramObj.verificationToken = token;
          return User.create(paramObj);
        })
        .then(function(user) {
          newUser = user;
          delete user.password;
          sails.log.debug(user);

          // create send email job
          let baseUrl = AppService.getBaseUrl();
          Jobs.now('SendMailJob', {
            view: 'verifyEmail',
            locals: {
              layout: '../layout',
              verificationURL: baseUrl + '/auth/verify?token=' + user.verificationToken,
            },
            options: {
              to: user.email,
              subject: 'Verify your email',
            }
          });

          return res.ok();
        })
        .catch(function (err) {
          sails.log.error(err);
          return res.send(500, err.message);
        });
    }

    else {
      return res.notFound();
    }
  },


  verify: function (req, res) {
    if (req.method == 'GET') {
      let token = req.param('token');
      if (!token) return res.badRequest('No token found', '500');
      User
        .findOne({
          verificationToken: token
        })
        .then(function (user) {
          if (!user) {
            throw new Error('Invalid or expired token:' + token);
          } else {
            sails.log.info('Verified token: ' + token);
            user.verified = true;
            user.verificationToken = null;
            // generate wallet addresses
            for (let cur in user.deposits) {
              let walletObj = BlockChainService.generateWallet(cur);
              user.deposits[cur] = {
                wallet: walletObj.address,
                privateKey: walletObj.privateKey,
              }
            }
            return User.update(user.id, user);
          }
        })
        .then(function (updates) {
          sails.log.debug(updates);
          // investor's ETH wallet address
          Jobs.now('SendMailJob', {
            view: 'walletEmail',
            locals: {
              layout: '../layout',
              'walletURL': AppService.getBaseUrl() + '/page/investor?wallet=' + updates[0].wallet
            },
            options: {
              to: updates[0].email,
              subject: "Your Investor Page",
            }
          });

          return res.view({
            layout: false,
            investor: updates[0],
            profileURL: AppService.getBaseUrl() + '/page/investor?wallet=' + updates[0].wallet
          });

        })
        .catch(function (err) {
          sails.log.error(err);
          req.session.flash = {
            err: err.message
          };
          return res.redirect('/');
        });
    }

    else {
      return res.notFound();
    }
  },


  /**
   * Resend investor page link to valid email
   */
  resend: function (req, res) {
   if (req.method == 'GET') {
      let currencies = {};
      ParamService
        .getAcceptedCurrenciesAndCurentPhase()
        .then(function (param) {
          currencies = param.acceptedCurrencies;
          sails.log(currencies);
          res.view({
            currencies: currencies
          });
        })
        .catch(function (err) {
          sails.log.error(err);
          res.view({
            currencies: currencies
          });
        });
    }else if (req.method == 'POST') {
      let params = req.params.all();
      User
        .findOne({email: params.email, role: User.roles.investor})
        .then(function (user) {
          sails.log.debug(user);

          if (!user) {
            return res.notFound();
          }

          // investor's ETH wallet address
          Jobs.now('SendMailJob', {
            view: 'walletEmail',
            locals: {
              layout: '../layout',
              'walletURL': AppService.getBaseUrl() + '/page/investor?wallet=' + user.wallet
            },
            options: {
              to: user.email,
              subject: "Your Investor Page",
            }
          });

          return res.ok();
        })
        .catch(function (err) {
          sails.log.error(err);
          return res.send(500, err.message);
        });
    } else {
      res.notFound();
    }
  },

};
