
$(document).ready(function () {

    var d = $('#data-table-investors').DataTable({
        'searching': false,
        'lengthChange': false,
        'ordering': true,
        'processing': true,
        'serverSide': true,
        'ajax': '/user/investorList',
        'columnDefs': [{
            'searchable': false,
            'targets': 0
        }],
        'columns': [
            { 
                'data': null,
                'render': function (data, type, full, meta) {
                    return d.page.info().start + meta.row + 1;
                }, 
                'responsivePriority': 1
            },
            { 'data': 'id', 'visible': false },
            { 'data': 'email', 'orderable': true, 'responsivePriority': 3 },
            { 'data': 'wallet', 'orderable': false, 'responsivePriority': 4 },
            { 'data': 'createdAt', 'orderable': true, 'responsivePriority': 5 },
            { 'data': 'bitcoin', 'className': 'none', 'defaultContent': 'N/A' },
            { 'data': 'bitcoinCash', 'className': 'none', 'defaultContent': 'N/A' },
            { 'data': 'dash', 'className': 'none', 'defaultContent': 'N/A' },
            { 'data': 'ether', 'className': 'none', 'defaultContent': 'N/A' },
            { 'data': 'litecoin', 'className': 'none', 'defaultContent': 'N/A' },
            {
                'data': null,
                'responsivePriority': 2,
                'render': function (data, type, full, meta) {
                    return '<button title="Send Token to Investor" data-id="' + full.id + '" class="btn btn-success btn-contribute-coins" role="button"><i class="material-icons">copyright</i></button>';
                }
            },            
        ]
    });


    // on datatable load
    d.on('draw', function() {

        // bind contribute coins click
        $('.btn-contribute-coins').off().on('click', contributeCoinsHandler);

    });

    d.on('responsive-display', function (e, datatable, row, showHide, update) {
        // bind button click handlers
        if (showHide) {            

            // bind contribute coins click
            $('.btn-contribute-coins').off().on('click', contributeCoinsHandler);

            // forward money
            $('.btn-forward').on('click', function () {
                let button = $(this);
                button.prop('disabled', true);
                let investorId = $(this).data('investor-id');
                let currency = $(this).data('currency');
                let data = {
                    investorId: investorId,
                    currency: currency,
                }
                $.post('/user/forwardFunds', data, function (res) {
                    console.log(res);
                    let msg = 'Transfer ' + res.amount + ' from ' + res.from + ' (' + res.currency + ') to ' + res.to + '?';
                    if (!confirm(msg)) {
                        button.prop('disabled', false);    
                    } else {
                        data.confirmed = true;
                        $.post('/user/forwardFunds', data, function (res) {
                            console.log(res);
                            msg = 'Successfully transfer ' + res.amount + ' from ' + res.from + ' (' + res.currency + ') to ' + res.to + '. Tx hash: ' + res.hash;
                            alert(msg);
                        })
                        .fail(function (xhr, status, error) {
                            console.error(error);
                            alert(error);
                            button.prop('disabled', false);
                        });
                    }
                })
                .fail(function(xhr, status, error) {
                    console.error(error);
                    alert(error);                                        
                    button.prop('disabled', false);
                });
                return false;
            });


            // update balance
            $('.btn-update').on('click', function () {
                let button = $(this);
                button.prop('disabled', true);
                let investorId = $(this).data('investor-id');
                let currency = $(this).data('currency');
                let data = {
                    investorId: investorId,
                    currency: currency,
                }
                $.post('/user/updateBalance', data, function (res) {
                    alert(JSON.stringify(res, null, 2));
                    button.prop('disabled', false);
                })
                .fail(function (xhr, status, error) {
                    console.error(error);
                    alert(error);
                    button.prop('disabled', false);
                });
                return false;
            });            
        }
    });

    // load current rate
    $.getJSON('/page/exchangeRates', function (res) {
        $('.exchange-rates').empty();
        let rates = res.acceptedCurrencies;
        let exchangeRateList = [];   
        for (let key in rates) {
            let rate = rates[key];
            exchangeRateList.push(
                $('<div>').attr('class', 'col-md-6')
                    .append($('<div>').attr('class', 'media-left media-body').text('1 ' + rate.symbol + ' = ' + rate.rateToETH.toFixed(4) + ' ETH'))
                    .append($('<div>').attr('class', 'media-middle list-group-item-text').text('Last update: ' + moment(rate.lastUpdatedAt).fromNow()))
            );
        }
        $('.exchange-rates').append(exchangeRateList);
    })
    .fail(function (err) {
        alert('Failed to load exchange rates');
        console.error(err);
    });


});


function contributeCoinsHandler() {
    let button = $(this);
    let investorId = $(this).data('id');
    let data = {
        investorId: investorId
    }
    button.prop('disabled', true);
    $.post('/user/contributeCoins', data, function (res) {
        let msg = 'Send ' + res.totalToken + ' HME to ' + res.email + ' (' + res.wallet + ')?\n' + JSON.stringify(res.deposits, null, 2);
        if (!confirm(msg)) {
            button.prop('disabled', false);
        } else {
            data.confirmed = true;
            $.post('/user/contributeCoins', data, function (res) {
                console.log(res);
                alert(JSON.stringify(res, null, 2));
                button.prop('disabled', false);
            })
                .fail(function (xhr, status, error) {
                    console.error(error);
                    alert(error.responseText || error || 'Error occured. Please try again or contact administrator');
                    button.prop('disabled', false);
                });
        }
    })
        .fail(function (xhr, status, error) {
            console.error(error);
            alert(error.responseText || error || 'Error occured. Please try again or contact administrator');
            button.prop('disabled', false);
        });
    return false;
}