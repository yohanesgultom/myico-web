/**
 * Investment summary job
 */
module.exports = function(agenda) {
    var job = {

        // execute job
        run: function(job, done) {
            sails.log.debug(job.attrs.name + ' is running..');
            let cacheKey = 'investmentSummary';
            let totalUSD = 0.0;
            let totalToken = 0.0;
            let currencyMap = {};
            let acceptedCurrencies = {};
            let summary = {};
            Param
                .findOne({
                    key: 'acceptedCurrencies'
                })
                .then(function(param) {
                    acceptedCurrencies = param.value;
                    return Transaction.find({value: {'>': 0}});
                })
                .then(function(transactions) {
                    // init currencyMap
                    for (let key in acceptedCurrencies) {
                        currencyMap[key] = {
                            value: 0.0,
                            valueUSD: 0.0
                        }
                    }

                    // summarize all transactions
                    for (let i = 0; i < transactions.length; i++) {
                        let tx = transactions[i];

                        // count grand total
                        totalUSD += tx.valueUSD;
                        totalToken += tx.valueToken;

                        // count total per currency
                        currencyMap[tx.currency].value += tx.value;
                        currencyMap[tx.currency].valueUSD += tx.valueUSD;
                    }

                    // gotta cache them all
                    summary = {
                        totalUSD: totalUSD,
                        totalToken: totalToken,
                        distributions: currencyMap,
                        count: transactions.length,
                    };
                    return Cache.findOne({ key: cacheKey });
                })
                .then(function (cache) {
                    if (cache) {
                        // update existing
                        sails.log.debug(job.attrs.name + ' is updating cache.');
                        cache.value = summary;
                        return Cache.update(cache.id, cache);
                    } else {
                        // create new one
                        sails.log.debug(job.attrs.name + ' is creating cache.');
                        return Cache.create({
                            key: cacheKey,
                            value: summary,
                        });
                    }
                })
                .then(function() {
                    sails.log.debug(job.attrs.name + ' is completed.');
                    done();
                })
                .catch(function(err) {
                    sails.log.error(job.attrs.name + ' hit error.');
                    sails.log.error(err);
                    done(err);
                });
        },
    };
    return job;
}