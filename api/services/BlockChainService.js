/**
 * BlockChainService.js
 * Handle blockchain API calls
 **/

// debug 3rd party API URLs
sails.log.debug('infura url: ' + sails.config.thirdParty.infura.url);
sails.log.debug('blockCypher url: ' + JSON.stringify(sails.config.thirdParty.blockCypher.url));

const
  Web3 = new require('web3'),
  web3 = new Web3(new Web3.providers.HttpProvider(sails.config.thirdParty.infura.url)),
  keythereum = require('keythereum'),
  Tx = require('ethereumjs-tx'),
  ethereum_address = require('ethereum-address');

  // bitcoin, bitcoin cash & litecoin
const bitcoin = require('bitcoinjs-lib');
require('bitcoinjs-testnets').register(bitcoin.networks);

  // dash
const bitcore = require('bitcore-lib-dash');

  // general
const
  uuid = require('uuid/v1'),
  crypto = require('crypto'),
  request = require('request'),
  util = require('util'),
  algorithm = sails.config.cryptoAlgo || 'aes-256-ctr',
  secret = sails.config.cryptoSecret || 'ryqPSybc6zgZyhsGJIRV8cvYuIgyCd';

// set networks
const bitcoinNetwork = sails.config.environment == 'production' ? bitcoin.networks.bitcoin : bitcoin.networks.testnet;
const litecoinNetwork = sails.config.environment == 'production' ? bitcoin.networks.litecoin : bitcoin.networks.litecoin_testnet;
bitcore.Networks.defaultNetwork = sails.config.environment == 'production' ? bitcore.Networks.livenet : bitcore.Networks.testnet;


/**
 * Encrypt plain text
 * @param {*} text 
 */
const encrypt = function (text) {
  let cipher = crypto.createCipher(algorithm, secret);
  let crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}


/**
 * Decrypt cipher text
 * @param {*} text 
 */
const decrypt = function (text) {
  let decipher = crypto.createDecipher(algorithm, secret);
  let dec = decipher.update(text, 'hex', 'utf8');
  dec += decipher.final('utf8');
  return dec;
}


/**
 * Send ether using Infura
 * @param {*} amount 
 * @param {*} to 
 * @param {*} from 
 * @param {*} privateKey 
 */
const sendEther = function (amount, to, from, privateKey, subtractFeeFromAmount) {
  subtractFeeFromAmount = subtractFeeFromAmount || false;
  if (!from.startsWith('0x')) from = '0x' + from;
  if (!to.startsWith('0x')) to = '0x' + to;
  let rawTx = {};
  // convert private key to byte buffer      
  let pk = new Buffer(privateKey, 'hex');
  sails.log.debug('Checking recommended gas price');
  // get recommended gas price
  return new Promise(function(resolve, reject) {
    web3.eth.getGasPrice(function(err, result) {
      if (err) {
        reject(err);
      } else {
        resolve(result);
      }
    });
  })
  .then(function(gasPrice) {
    sails.log.debug('Recommended gas price: ' + gasPrice);
    let wei = web3.toWei(amount, 'ether');
    rawTx = {
      gasPrice: gasPrice,
      to: to,
      value: wei,
    }
    // get transaction estimated gas amount
    sails.log.debug('Raw TX: ' + JSON.stringify(rawTx));
    return new Promise(function(resolve, reject) {
      web3.eth.estimateGas(rawTx, function(err, result) {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }        
      });
    });
  })
  .then(function(gas) {
    sails.log.debug('Estimated required gas: ' + gas);
    // subtract gas from amount
    let wei = rawTx.value;
    if (subtractFeeFromAmount) {
      wei = wei - rawTx.gasPrice * gas;
    }    
    rawTx.gas = gas;
    rawTx.value = wei;
    // get nonce    
    return new Promise(function(resolve, reject) {
      web3.eth.getTransactionCount(from, function(err, result) {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }                    
      });
    });    
  })
  .then(function(nonce) {
    sails.log.debug('Nonce: ' + nonce);
    rawTx.nonce = nonce;
    sails.log.debug('sending raw tx: ' + JSON.stringify(rawTx));
    return new Promise(function (resolve, reject) {
      // sign transaction using private key
      let tx = new Tx(rawTx);
      tx.sign(pk);
      let serializedTx = tx.serialize();

      // add 0x on serialized tx to prevent error
      web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function (err, hash) {
        if (err) {
          sails.log.error('sendRawTransaction error: ' + err.message);
          reject(err);
        } else {
          resolve(hash);
        }
      });
    });    
  });

}


/**
 * Send bitcoin using BlockCypher
 * @param {number} amount 
 * @param {string} to 
 * @param {string} from 
 * @param {string} wif 
 */
const sendBitcoin = function (amount, to, from, wif) {
  let keys = bitcoin.ECPair.fromWIF(wif, bitcoinNetwork);
  return new Promise(function (resolve, reject) {
    // create tx skeleton
    request.post({
      url: sails.config.thirdParty.blockCypher.url.bitcoin + '/txs/new',
      body: JSON.stringify({
        inputs: [{ addresses: [from] }],
        outputs: [{ addresses: [to], value: Math.round(amount * Math.pow(10, 8)) - 89900 }] // substract static fee to prevent insufficient fund
      }),
    },
      function (err, res, body) {
        if (err) {
          reject(err);
        } else {

          let tmptx = undefined;
          try {
            tmptx = JSON.parse(body);
            if (tmptx.errors) {
              sails.log.error(tmptx.errors);
              throw new Error('Error(s) found in response');
            } 
          } catch (e) {
            sails.log.error('Unexpected response: ' + body);
            reject(e);
          } 

          if (tmptx && !tmptx.errors) {
            // signing each of the hex-encoded string required to finalize the transaction
            tmptx.pubkeys = [];
            tmptx.signatures = tmptx.tosign.map(function (tosign, n) {
              tmptx.pubkeys.push(keys.getPublicKeyBuffer().toString('hex'));
              return keys.sign(new Buffer(tosign, 'hex')).toDER().toString('hex');
            });

            sails.log.debug('Signed tx: ' + JSON.stringify(tmptx));
            // sending back the transaction with all the signatures to broadcast
            request.post({
              url: sails.config.thirdParty.blockCypher.url.bitcoin + '/txs/send',
              body: JSON.stringify(tmptx),
            },
              function (err, res, body) {
                if (err) {
                  reject(err);
                } else {
                  try {
                    sails.log.debug('sendBitcoin res: ' + body);
                    let finaltx = JSON.parse(body);
                    resolve(finaltx.tx.hash);
                  } catch (e) {
                    sails.log.error('Unexpected response: ' + body);
                    reject(e);
                  }
                }
              }
            );
          }
        }
      }
    );
  });
}


/**
 * Send dash coin using SoChain
 * @param {number} amount 
 * @param {string} to 
 * @param {string} from 
 * @param {string} wif 
 */
const sendDash = function (amount, to, from, wif) {
  let pk = bitcore.PrivateKey.fromWIF(wif);
  return new Promise(function (resolve, reject) {
    // TODO:
    resolve(pk);
  });
};


/**
 * Single address: https://api.etherscan.io/api?module=account&action=balance&address=0xde0b295669a9fd93d5f28d9ec85e40f4cb697bae&tag=latest&apikey=YourApiKeyToken
 * Multiple addresses: https://api.etherscan.io/api?module=account&action=balancemulti&address=0xddbd2b932c763ba5b1b7ae3b362eac3e8d40121a,0x63a9975ba31b0b9626b34300f7f627147df1f526,0x198ef1ec325a96cc354c7266a038be8b5c558f67&tag=latest&apikey=YourApiKeyToken
 * @param {*} address 
 */
const checkEtherBalance = function (address) {
  let url = sails.config.thirdParty.etherscan.url;
  let qs = {
    module: 'account',
    action: 'balance',
    tag: 'latest',
    address: !address.startsWith('0x') ? '0x' + address : address,
    apiKey: sails.config.thirdParty.etherscan.apiKey,
  }
  return new Promise(function (resolve, reject) {
    request.get({ url: url, qs: qs }, function (err, res) {
      if (err) {
        reject(err);
      } else if (!res) {
        reject('Empty response');        
      } else {
        try {
          let wei = JSON.parse(res.body).result;
          resolve(web3.fromWei(wei));
        } catch (e) {
          reject(e);
        }                
      }
    });
  });
}


const checkBlockTrailBalance = function (type, address) {
  // https://api.blocktrail.com/v1/tbcc/address/mueSxXDeD9ukFHysooPMgP354WPMghdgbr?api_key=MY_APIKEY
  let apiKey = sails.config.thirdParty.blockTrail.apiKey;
  let url = sails.config.thirdParty.blockTrail.url[type] + '/address/' + address;
  let qs = { api_key: apiKey };
  return new Promise(function (resolve, reject) {
    request.get({ url: url, qs: qs }, function (err, res) {
      if (err) {
        reject(err);
      } else if (!res) {
        reject('Empty response');
      } else {
        try {
          let body = JSON.parse(res.body);
          let satoshi = parseFloat(body.balance);
          resolve((satoshi * 0.00000001).toString());
        } catch (e) {
          sails.log.error('Error when parsing: ' + res.body);
          reject(e);
        }
      }
    });
  });
}


const checkSoChainBalance = function (type, address) {
  let network = sails.config.thirdParty.soChain.network[type];
  let url = sails.config.thirdParty.soChain.url + '/get_address_balance/' + network + '/' + address;
  if (!network) return Promise.reject(type + ' is not supported');
  return new Promise(function (resolve, reject) {
    request.get(url, { timeout: 5000 }, function (err, res) {
      if (err) {
        reject(err);
      } else if (!res) {
        reject('Empty response');
      } else {
        let body = JSON.parse(res.body);
        sails.log.debug(body);
        if (body.data) {
          resolve(body.data.confirmed_balance);
        } else {
          reject('Unexpected API response');
        }
      }
    });
  });
}


const checkBlockCypherBalance = function (type, address) {
  let url = sails.config.thirdParty.blockCypher.url[type] + '/addrs/' + address + '/balance';
  return new Promise(function (resolve, reject) {
    request.get(url, { timeout: 5000 }, function (err, res) {
      if (err) {
        reject(err);
      } else if (!res) {
        reject('Empty response');
      } else {
        let body = JSON.parse(res.body);
        if (body.error) {
          reject(body.error);
        } else {
          let satoshi = parseFloat(body.balance);
          resolve((satoshi * 0.00000001).toString());
        }
      }
    });
  });
}

const checkBitcoinBalance = function (address) {
  return checkBlockCypherBalance('bitcoin', address);
}


const checkLitecoinBalance = function (address) {
  return checkSoChainBalance('litecoin', address);
}


const checkDashBalance = function (address) {
  return checkSoChainBalance('dash', address);
}


const checkBitcoinCashBalance = function (address) {
  return checkBlockTrailBalance('bitcoinCash', address);
}

/**
 * Generate 16 bytes Buffer in hex format
 */
const rng = function () {
  return Buffer.from(crypto.randomBytes(16).toString('hex'));
}


module.exports = {

  encrypt: encrypt,

  decrypt: decrypt,

  generateWallet: function (type) {

    if (type == 'ether') {
      let dk = keythereum.create(uuid());
      let keyObject = keythereum.dump(uuid(), dk.privateKey, dk.salt, dk.iv);
      keyObject.privateKey = dk.privateKey.toString('hex');
      return keyObject;

    } else if (type == 'bitcoin') {
      let keyPair = bitcoin.ECPair.makeRandom({ network: bitcoinNetwork, rng: rng });
      let wif = keyPair.toWIF();
      let address = keyPair.getAddress();
      return {
        address: address,
        // it is easier to use the wif for transaction
        // reference: https://github.com/bitcoinjs/bitcoinjs-lib/issues/416
        privateKey: wif,
      }

    } else if (type == 'dash') {
      let value = rng();
      let hash = bitcore.crypto.Hash.sha256(value);
      let bn = bitcore.crypto.BN.fromBuffer(hash);
      let privateKey = new bitcore.PrivateKey(bn);
      return {
        address: privateKey.toAddress().toString(),
        privateKey: privateKey.toWIF(),
      }

    } else if (type == 'bitcoinCash') {
      let keyPair = bitcoin.ECPair.makeRandom({ network: bitcoinNetwork, rng: rng });
      let wif = keyPair.toWIF();
      let address = keyPair.getAddress();
      return {
        address: address,
        privateKey: wif,
      }

    } else if (type == 'litecoin') {
      let keyPair = bitcoin.ECPair.makeRandom({ network: litecoinNetwork, rng: rng });
      let wif = keyPair.toWIF();
      let address = keyPair.getAddress();
      return {
        address: address,
        privateKey: wif,
      }

    } else {
      sails.log.error('Crypto type is not yet supported: ' + type);
      return {
        address: 'not-yet-suported',
        privateKey: 'not-yet-suported'
      };
    }
  },

  getCurrentExchangeRate: function (cur1, cur2) {
    let fromSymbol = cur1.toUpperCase();
    let toSymbols = cur2 instanceof Array ? cur2.join(',').toUpperCase() : cur2.toUpperCase();
    let url = util.format(
      sails.config.thirdParty.cryptoCompare.url + '/data/price?fsym=%s&tsyms=%s',
      fromSymbol,
      toSymbols
    );
    return new Promise(function (resolve, reject) {
      request.get(url, { timeout: 5000 }, function (err, res) {
        if (err) {
          reject(err);
        } else if (!res || res == undefined) {
          reject('Invalid response: ' + res);
        } else {
          resolve(JSON.parse(res.body));
        }
      });
    });
  },

  getExchangeRate: function (cur1, cur2, timestamp) {
    let url = sails.config.thirdParty.cryptoCompare.url + '/data/pricehistorical?fsym=' + cur1.toUpperCase() + '&tsyms=' + cur2.toUpperCase() + '&ts=' + timestamp;
    return new Promise(function (resolve, reject) {
      request.get(url, { timeout: 5000 }, function (err, res) {
        if (err) {
          reject(err);
        } else if (!res) {
          reject('Empty response');
        } else {
          resolve(JSON.parse(res.body)[cur1]);
        }
      });
    });
  },

  checkEthereumAddr: function (addr) {
    if (ethereum_address.isAddress(addr)) {
      return true;
    } else {
      return false;
    }
  },


  /**
   * Send cryptocurrency
   */
  sendTransaction: function (type, amount, to, from, privateKey) {
    if (type == 'ether') {      
      return sendEther(amount, to, from, privateKey, true);
    } else if (type == 'bitcoin') {
      return sendBitcoin(amount, to, from, privateKey);
    } else if (type == 'dash') {
      return sendDash(amount, to, from, privateKey);
    } else {
      // TODO: bitcoinCash, litecoin
      return Promise.reject(type + ' is not yet supported');
    }
  },


  /**
   * Check balance using public API
   */
  checkBalance: function (type, address) {
    if (type == 'ether') {
      return checkEtherBalance(address);
    } else if (type == 'bitcoin') {
      return checkBitcoinBalance(address);
    } else if (type == 'litecoin') {
      return checkLitecoinBalance(address);
    } else if (type == 'dash') {
      return checkDashBalance(address);
    } else if (type == 'bitcoinCash') {
      return checkBitcoinCashBalance(address);
    } else {
      return Promise.reject(type + ' is not yet supported');
    }
  }
}
