const async = require('asyncawait/async');
const await = require('asyncawait/await');
const path = require('path');
const fs = require('fs');
const TruffleContract = require('truffle-contract');
const Web3 = require('web3');
const ethereumjsWallet = require('ethereumjs-wallet');
const ProviderEngine = require('web3-provider-engine');
const WalletSubprovider = require('web3-provider-engine/subproviders/wallet.js');
const Web3Subprovider = require('web3-provider-engine/subproviders/web3.js');
const FilterSubprovider = require('web3-provider-engine/subproviders/filters.js');

// constants
const RPC_SERVER = sails.config.thirdParty.infura.url;
const HOLDME_CONTRACT = path.resolve(__dirname, '..', '..', 'contracts', 'Holdme.json');
const HOLDME_TOKEN_SALE_CONTRACT = path.resolve(__dirname, '..', '..', 'contracts', 'HoldmeTokenSale.json');


// web3 provider and client
const provider = new Web3.providers.HttpProvider(RPC_SERVER);
const web3 = new Web3(provider);

// set infura.io custom provider for truffle-contract self-signing
const privateKey = sails.config.smartContract.whitelisted.privateKey;
const wallet = ethereumjsWallet.fromPrivateKey(new Buffer(privateKey, 'hex'));
const whitelistAddress = '0x' + wallet.getAddress().toString('hex');
const engine = new ProviderEngine();
engine.addProvider(new FilterSubprovider());
engine.addProvider(new WalletSubprovider(wallet, {}));
engine.addProvider(new Web3Subprovider(new Web3.providers.HttpProvider(RPC_SERVER)));
engine.start(); // Required by the provider engine.


/**
 * Load contract from file
 * @param {*} file 
 */
function loadContract(file, provider, address) {
    return new Promise(function (resolve, reject) {
        fs.readFile(file, 'utf-8', function (err, data) {
            if (err) {
                reject(err);
            } else {
                let contract = TruffleContract(JSON.parse(data));
                contract.setProvider(provider);
                contract.defaults({ from: address, gas: 4500000 });
                resolve(contract);
            }
        });
    });
}


/**
 * Get totalTokenIssued from HOLDME_TOKEN_SALE_CONTRACT
 */
const totalTokenIssued = async(function () {
    let holdmeTokenSaleContract = await(loadContract(HOLDME_TOKEN_SALE_CONTRACT, engine, whitelistAddress));
    let holdmeTokenSale = await(holdmeTokenSaleContract.deployed());
    let totalTokenIssued = await(holdmeTokenSale.totalTokenIssued());
    return totalTokenIssued.toString();
});


/**
 * Get whitelisted addresses
 */
const getWhitelists = async(function () {
    let holdmeTokenSaleContract = await(loadContract(HOLDME_TOKEN_SALE_CONTRACT, engine, whitelistAddress));
    let holdmeTokenSale = await(holdmeTokenSaleContract.deployed());
    let whitelists = await(holdmeTokenSale.getWhitelists());
    return whitelists;
});


/**
 * Get smartcontract whitelist balance
 */
const getWhitelistBalance = async(function () {
    let balance = await(web3.eth.getBalance(whitelistAddress));
    return balance.toString();
});


/**
 * contribute coins to HOLDME_TOKEN_SALE_CONTRACT
 */
const contributeCoins = async(function (contributorAddress, amount) {
    let weiAmount = new web3.BigNumber(web3.toWei(amount, 'ether'));
    sails.log.debug(`contributeCoins weiAmount = ${weiAmount}`);
    let holdmeTokenSaleContract = await(loadContract(HOLDME_TOKEN_SALE_CONTRACT, engine, whitelistAddress));
    let holdmeTokenSale = await(holdmeTokenSaleContract.deployed());
    let contributeCoins = await(holdmeTokenSale.contributeCoins(contributorAddress, weiAmount, { from: whitelistAddress }));
    return contributeCoins;
});


module.exports = {
    totalTokenIssued: totalTokenIssued,
    getWhitelists: getWhitelists,
    getWhitelist: function() { return whitelistAddress; },
    getWhitelistBalance: getWhitelistBalance,
    contributeCoins: contributeCoins,
}