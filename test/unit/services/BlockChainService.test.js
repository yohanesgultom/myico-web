const chai = require('chai'),
  moment = require('moment'),
  should = chai.should(),
  expect = chai.expect;

describe('BlockChainService', function() {

  describe('#encrypt #decrypt', function() {
    it('should be able to encrypt and decrypt', function (done) {
      let text = 'somesecret';
      let encrypted = BlockChainService.encrypt(text);
      let decrypted = BlockChainService.decrypt(encrypted);
      encrypted.should.equal('9655a29792acb816b37e');
      decrypted.should.equal(text);
      done();
    });
  });


  describe('#generateWallet', function() {
    it('should generate Ether wallet and private key', function (done) {
      let wallet = BlockChainService.generateWallet('ether');
      wallet.address.length.should.be.above(16);
      wallet.privateKey.length.should.be.above(16);
      done();
    });

    it('should generate Bitcoin wallet and private key', function (done) {
      let wallet = BlockChainService.generateWallet('bitcoin');
      wallet.address.length.should.be.above(16);
      wallet.privateKey.length.should.be.above(16);
      done();
    });

    it('should generate BitcoinCash wallet and private key', function (done) {
      let wallet = BlockChainService.generateWallet('bitcoinCash');
      wallet.address.length.should.be.above(16);
      wallet.privateKey.length.should.be.above(16);
      done();
    });

    it('should generate Dash wallet and private key', function (done) {
      let wallet = BlockChainService.generateWallet('dash');
      wallet.address.length.should.be.above(16);
      wallet.privateKey.length.should.be.above(16);
      done();
    });

    it('should generate Litecoin wallet and private key', function (done) {
      let wallet = BlockChainService.generateWallet('litecoin');
      wallet.address.length.should.be.above(16);
      wallet.privateKey.length.should.be.above(16);
      done();
    });

  });

});
