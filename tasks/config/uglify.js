/**
 * `uglify`
 *
 * ---------------------------------------------------------------
 *
 * Minify client-side JavaScript files using UglifyJS.
 *
 * For usage docs see:
 *   https://github.com/gruntjs/grunt-contrib-uglify
 *
 */
module.exports = function(grunt) {

  grunt.config.set('uglify', {
    dist: {
      options: {
        sourceMap: true
      },
      files: [
        {
          src: ['.tmp/public/js/app.js'],
          dest: '.tmp/public/js/app.min.js'
        },
        {
          src: ['.tmp/public/js/socialLinks.js'],
          dest: '.tmp/public/js/socialLinks.min.js'
        },
        {
          src: ['.tmp/public/vendors/propeller.js'],
          dest: '.tmp/public/vendors/propeller.min.js'
        }, 
      ]
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
};
