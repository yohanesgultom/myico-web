/**
 * AppService.js
 * Provides app-related service
 **/

module.exports = {

  getBaseUrl: function () {
    let baseUrl = sails.config.appUrl;
    if (sails.config.environment == 'development' 
      && sails.config.port
      && [80, 443].indexOf(sails.config.port) < 0) {
      baseUrl += ':' + sails.config.port;
    }
    return baseUrl;
  }

}
