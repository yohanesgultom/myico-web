/**
 * PageController
 *
 * @description :: Server-side logic for managing pages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const moment = require('moment');

module.exports = {

  index: function (req, res) {
    if (req.method == 'GET') {
      let currencies = {};
      let phase = {};
      let roadmap = [];
      let tokenSale = {};
      let summary = {};
      let now = moment();

      ParamService
        .getMultipleParams(['acceptedCurrencies', 'phases', 'roadmap', 'tokenSale'])
        .then(function (param) {
          currencies = param.acceptedCurrencies;
          phase = ParamService.getNextPhase(param.phases);
          roadmap = param.roadmap;
          tokenSale = param.tokenSale;
          return Cache.findOne({key: 'investmentSummary'});
        })
        .then(function (cache) {
          summary = cache.value;
          sails.log.debug({
            currencies: currencies,
            phase: phase,
            roadmap: roadmap,
            tokenSale: tokenSale,
            summary: summary,
          });
          res.view({
            currencies: currencies,
            phase: phase,
            roadmap: roadmap,
            tokenSale: tokenSale,
            summary: summary,
          });
        })
        .catch(function (err) {
          sails.log.error(err);
          res.view({
            currencies: currencies,
            phase: phase,
            roadmap: roadmap,
            tokenSale: tokenSale,
            summary: summary,
          });
        });
    } else {
      res.notFound();
    }
  },


  join: function (req, res) {
    if (req.method == 'GET') {
      let currencies = {};
      let investors = [];
      let phase = {};
      let now = moment();

      ParamService
        .getAcceptedCurrenciesAndCurentPhase()
        .then(function (param) {
          currencies = param.acceptedCurrencies;
          phase = param.currentPhase;

          // get top investors
          return User
            .find({ role: User.roles.investor })
            .sort('depositTotal DESC')
            .limit(100);
        })
        .then(function (users) {
          investors = users;
          res.view({
            currencies: currencies,
            investors: investors,
            phase: phase,
          });
        })
        .catch(function (err) {
          sails.log.error(err);
          res.view({
            currencies: currencies,
            investors: investors,
            phase: phase,
          });
        });
    } else {
      res.notFound();
    }
  },

  joinether: function (req, res) {
   if (req.method == 'GET') {
     let currencies = {};
     let investors = [];
     let phase = {};
     let now = moment();

     ParamService
       .getAcceptedCurrenciesAndCurentPhase()
       .then(function (param) {
         currencies = param.acceptedCurrencies;
         phase = param.currentPhase;

         // get top investors
         return User
           .find({ role: User.roles.investor })
           .sort('depositTotal DESC')
           .limit(100);
       })
       .then(function (users) {
         investors = users;
         res.view({
           currencies: currencies,
           investors: investors,
           phase: phase,
         });
       })
       .catch(function (err) {
         sails.log.error(err);
         res.view({
           currencies: currencies,
           investors: investors,
           phase: phase,
         });
       });
   } else {
     res.notFound();
   }
 },

  /**
   * Show investor page
   */
  investor: function(req, res, next) {
    if (req.method == 'GET') {
      let wallet = req.param('wallet');
      let currencies = {};
      let investor = {};

      // normalize wallet addreses format
      if (wallet.startsWith('0x')) {
        wallet = wallet.substring(2);
      }

      Param
        .findOne({ key: 'acceptedCurrencies' })
        .then(function (param) {
          currencies = param.value;
          return User.findOne({
            wallet: wallet,
            role: User.roles.investor
          });
        })
        .then(function (user) {
          investor = user;
          if (!investor) return res.notFound(undefined, '404');

          // block unverified investor
          if (!investor.verified) {
            req.session.flash = { err: 'Please check and follow instruction sent to your email to verify your account' };
            return res.redirect('/');
          }

          return res.view({
            currencies: currencies,
            investor: investor
          });
        })
        .catch(function (err) {
          sails.log.error(err);
          if (!investor) return res.notFound(undefined, '404');
          return res.view({
            currencies: currencies,
            investor: investor
          });
        });

    } else {
      return res.notFound(undefined, '404');
    }
  },


  /**
   * Get accepted currencies exchange rates
   */
  exchangeRates: function (req, res) {
    Param
      .findOne({ key: 'acceptedCurrencies' })
      .then(function (param) {
        return res.json({
          acceptedCurrencies: param.value
        });
      })
      .catch(function (err) {
        sails.log.error(err);
        return res.serverError(err);
      });
  },


  contactSend: function (req, res){
    if (req.method == 'GET') {
      // create send email job
      Jobs.now('SendMailJob', {
        view: 'contactEmail',
        locals: {
          'name': req.param('Name'),
          'email': req.param('Email'),
          'subject': req.param('Subject'),
          'message': req.param('message'),
        },
        options: {
          to: sails.config.email.from,
          subject: req.param('Subject'),
        }
      });

      res.json({"status":"OK"});
    }
  },


  checkEthAddr: function(req, res){
    if (req.method == 'GET') {
      if(BlockChainService.checkEthereumAddr(req.param('wallet'))){
        res.json({"status":"valid"});
      }else{
        res.json({"status":"invalid"});
      }
    }
  },


  /**
   * Check request header `cf-ipcountry` from CloudFlare (CF)
   * Obviously this is only available when site is accessed from CF
   */
  checkCountry: function (req, res) {
    let country = req.headers['cf-ipcountry'] ? req.headers['cf-ipcountry'].toUpperCase() : null;
    return res.json({ country: country });
  },


  /**
   * Get social media param
   */
  socialMedia: function (req, res) {
    Param
      .findOne({key: 'socialMedia'})
      .then(function (param) {
        return res.json(param.value);
      })
      .catch(function (err) {
        return res.notFound();
      });
  },

  /**
   * Add investor wallet
   */
  addWallet: function (req, res) {
    let params = req.params.all();
    User
      .findOne({ id: params.investorId, role: User.roles.investor })
      .then(function (user) {
        if (!user) {
          throw new Error('User not found');
        }
        if (user.deposits[params.newWallet]) {
          throw new Error('Only one wallet per currency allowed: ' + params.newWallet);
        }

        // try to generate key pair
        let keyPair = BlockChainService.generateWallet(params.newWallet);
        if (!keyPair || keyPair.address.length <= 16) {
          throw new Error('Currency not supported: ' + params.newWallet);
        } else {
          // save key pair
          user.deposits[params.newWallet] = {
            wallet: keyPair.address,
            privateKey: keyPair.privateKey,
          }
          return User.update(user.id, user);
        }
      })
      .then(function (updates) {
        if (!updates || updates.length <= 0) {
          throw new Error('Update failed');
        }
        return res.json({
          currency: params.newWallet,
          address: updates[0].deposits[params.newWallet].wallet,
        });
      })
      .catch(function (err) {
        return res.serverError(err);
      });
  }
};
