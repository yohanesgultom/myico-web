
$(document).ready(function () {

    var d = $('#data-table-transactions').DataTable({
        'searching': false,
        'lengthChange': false,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': '/transaction/list',
        'columnDefs': [{
            'searchable': false,
            'orderable': false,
            'targets': 0
        }],
        'columns': [
            { 
                'data': null,
                'render': function (data, type, full, meta) {
                    return d.page.info().start + meta.row + 1;
                }
            },
            { 'data': 'id', 'visible': false },
            { 'data': 'createdAt' },
            { 'data': 'userId' },
            { 'data': 'currency' },
            { 'data': 'value' },
            { 
                'data': null, 
                'render': function (data, type, full, meta) {
                    return '<a href="/transaction/edit/' + full.id + '" class="btn pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" role="button"><i class="material-icons">create</i></a>' +
                    '<a href="/transaction/destroy/' + full.id + '" class="btn pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-danger btn-sm" onclick="return confirm(\'Delete this record?\')" role="button"><i class="material-icons">clear</i></a>';
                }
            },
        ]
    });


});
