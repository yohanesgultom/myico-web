/**
 * Transaction.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    userId: {
      type: 'string',
      required: true
    },
    currency: {
      type: 'string',
      required: true
    },
    value: {
      type: 'float',
      required: true
    },    
    valueUSD: {
      type: 'float',
    },    
    valueETH: {
      type: 'float',
    },    
    valueToken: {
      type: 'float',
    },    
  },

  beforeValidate: function (values, cb) {
    if (values.value) {
      // calculate tx values in USD, ETH and our token
      ParamService
        .getAcceptedCurrenciesAndCurentPhase()
        .then(function (param) {
          currencies = param.acceptedCurrencies;
          phase = param.currentPhase;
          values.valueUSD = values.value * currencies[values.currency].rateToUSD;
          values.valueETH = values.value * currencies[values.currency].rateToETH;
          values.valueToken = values.value / currencies[values.currency].ratePerToken;          
          cb();
        });
    }    
  },
  
  afterCreate: function (obj, cb) {
    Jobs.now('InvestmentSummaryJob');
    cb();
  },

  afterUpdate: function (obj, cb) {
    Jobs.now('InvestmentSummaryJob');
    cb();
  },

  afterDestroy: function (obj, cb) {
    Jobs.now('InvestmentSummaryJob');
    cb();
  }

};
