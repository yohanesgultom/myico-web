$(document).ready(function () {

    // check balance
    var checkBalanceFormId = '#check-balance';
    $('button', checkBalanceFormId).click(function () {
        var btn = $(this);
        btn.prop('disabled', true);
        var data = {
            api: 'checkBalance',
            currency: $('[name=currency]', checkBalanceFormId).val(),
            address: $('[name=address]', checkBalanceFormId).val(),
        };
        if (data.currency && data.address) {
            $.post('/transaction/apiTest', data, function (res) {
                alert(JSON.stringify(res, null, 2));
            })
                .fail(function (err) {
                    alert(JSON.stringify(err, null, 2));
                })
                .always(function () {
                    btn.prop('disabled', false);
                });
        }
    });


    // totalTokenIssued
    var totalTokenIssuedFormId = '#total-token-issued';
    $('button', totalTokenIssuedFormId).click(function () {
        var btn = $(this);
        btn.prop('disabled', true);
        var data = {
            api: 'totalTokenIssued',
        };
        $.post('/transaction/apiTest', data, function (res) {
            alert(JSON.stringify(res, null, 2));
        })
            .fail(function (err) {
                alert(JSON.stringify(err, null, 2));
            })
            .always(function () {
                btn.prop('disabled', false);
            });
    });


    // contribute coins
    var contributeCoinsFormId = '#contribute-coins';
    $('button', contributeCoinsFormId).click(function () {
        var btn = $(this);
        btn.prop('disabled', true);
        var data = {
            api: 'contributeCoins',
            address: $('[name=address]', contributeCoinsFormId).val(),
            coins: $('[name=coins]', contributeCoinsFormId).val(),
        };
        if (data.address && data.coins) {
            $.post('/transaction/apiTest', data, function (res) {
                alert(JSON.stringify(res, null, 2));
            })
                .fail(function (err) {
                    alert(JSON.stringify(err, null, 2));
                })
                .always(function () {
                    btn.prop('disabled', false);
                });
        }
    });

});
