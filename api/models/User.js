/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

const bcrypt = require('bcrypt'),
  crypto = require('crypto');

const roles = {
  investor: 'investor',
  admin: 'admin'
};


module.exports = {

  roles: roles,

  attributes: {
    email: {
      type: 'string',
      unique: true,
      email: true,
      required: true
    },
    emailMasked: function () {
      return this.email.replace(/^(.)(.*)(@.*)$/, function (_, a, b, c) {
        return a + b.replace(/./g, '*') + c;
      });
    },
    password: {
      type: 'string',
      required: true,
      minLength: 5
    },
    role: {
      type: 'string',
      required: true,
      defaultsTo: roles.investor,
      enum: Object.keys(roles)
    },
    wallet: {
      type: 'string',
      minLength: 33,
      unique: true
    },
    deposits: {
      type: 'json',
      minLength: 1
    },
    verified: {
      type: 'boolean',
      defaultsTo: false,
    },
    verificationToken: {
      type: 'string',
    },
    registrationIP: {
      type: 'string',
    },
    registrationCountry: {
      type: 'string',
    },
    nonUSCitizenDeclaration: {
      type: 'boolean',
      defaultsTo: false,
    }
  },


  beforeValidate: function(values, cb) {
    if (values.email) {
      values.email = values.email.toLowerCase();
    }

    cb();
  },

  afterValidate: function (values, cb) {
    let promises = [];

    if (values.password) {
      promises.push(new Promise(function (resolve, reject) {
        bcrypt.hash(values.password, 10, function (err, hash) {
          if (err) reject(err);
          values.password = hash;
          resolve(values);
        });
      }));
    } else {
      delete values.password;
    }

    Promise.all(promises)
      .then(function (res) {
        cb();
      })
      .catch(function (err) {
        cb(err);
      });
  },


  comparePassword: function(plain, hash) {
    return bcrypt.compareSync(plain, hash);
  },


  generateVerificationToken: function () {
    return new Promise(function (resolve, reject) {
      crypto.randomBytes(20, function (err, buf) {
        if (err) reject(err);
        resolve(buf.toString('hex'))
      });
    })
  },

};
