/**
 * Send mail job
 */
module.exports = function(agenda) {
    var job = {

        /* 
        Data structure example:

        {
          // name of the ejs view directory under `views/emailTemplates`
          view: 'verifyEmail',

          // local variables needed by the ejs template
          locals: {
            // mandatory: relative path to ejs layout
            // set `layout:` false to disable
            layout: '../layout',

            // add any variables you like
            verificationURL: baseUrl + '/auth/verify?token=' + user.verificationToken,
          },

          // email options
          options: {
            // mandatory: recipient email address
            to: user.email,

            // mandatory: email subject
            subject: 'Verify your email',

            // optional: sender email address
            // if not available, system will look from config
            from: sails.config.email.from
          }
        }
        */

        // execute job
        run: function(job, done) {
            sails.log.debug(job.attrs.name + ' is running..');
            let data = job.attrs.data;
            new Promise(function(resolve, reject) {
                    sails.hooks.email.send(
                        data.view,
                        data.locals,
                        data.options,
                        function(err, result) {
                            if (err) reject(err);
                            resolve(result);
                        }
                    );
                })
                .then(function(result) {
                    sails.log.debug(job.attrs.name + ' email successfully sent to ' + data.options.to);
                    done();
                })
                .catch(function(err) {
                    sails.log.error(job.attrs.name + ' hit error.');
                    sails.log.error(err);
                    done(err);
                });
        },
    };
    return job;
}