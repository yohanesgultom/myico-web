/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    'new': function(req, res) {
        let acceptedCurrencies = {};
        Param.findOne({
                key: 'acceptedCurrencies'
            }).then(function(param) {
                acceptedCurrencies = param.value;
                res.view({
                    acceptedCurrencies: acceptedCurrencies
                });
            })
            .catch(function(err) {
                sails.log.error(err.message);
                res.view({
                    acceptedCurrencies: acceptedCurrencies
                })
            });
    },

    index: function(req, res) {
        res.view();
    },

    create: function(req, res) {

        var paramObj = {
            email: req.param('email'),
            password: req.param('password'),
            role: req.param('role'),
            wallet: req.param('wallet'),
        }

        let currencies = req.param('currencies');
        if (!currencies || currencies.length < 0) {
            req.session.flash = {
                err: 'Please select one or more deposit currencies'
            };
            return res.redirect('/user/new');
        }

        paramObj.deposits = {};
        currencies.forEach(function(cur) {
            let walletObj = BlockChainService.generateWallet(cur);
            paramObj.deposits[cur] = {
                wallet: walletObj.address,
                privateKey: walletObj.privateKey
            }
        });

        sails.log.debug(paramObj);

        // Create a User with the params sent from
        // the sign-up form --> new.ejs
        User.create(paramObj, function(err, user) {
            if (err) {
                sails.log.error(err);
                req.session.flash = {
                    err: err
                };
                return res.redirect('/user/new');
            }
            // res.json(user);
            res.redirect('/user');

        });
    },

    list: function(req, res, next) {
        // exclude current user
        let start = req.param('start') || 0;
        let limit = req.param('length') || 10;
        let count = 0;
        let conditions = {
            id: {
                '!': req.session.user.id
            }
        };
        User
            .count(conditions)
            .then(function(res) {
                count = res;
                return User
                    .find(conditions)
                    .sort('role ASC')
                    .sort('email ASC')
                    .limit(limit)
                    .skip(start);
            })
            .then(function(users) {
                return res.json({
                    draw: req.param('draw'),
                    data: users,
                    recordsFiltered: count,
                    recordsTotal: count,
                });
            })
            .catch(function(err) {
                sails.log.error(err);
                if (err) return next(err);
            });
    },


    edit: function(req, res, next) {

        User.findOne(req.param('id'), function(err, user) {
            if (err) return next(err);
            if (!user) return next('user doesn\'t exist.');
            res.view({
                u: user
            });
        });
    },

    update: function(req, res, next) {

        var paramObj = {
            email: req.param('email'),
            password: req.param('password'),
            role: req.param('role'),
            wallet: req.param('wallet'),
            deposits: req.param('deposits'),
        }

        // don't replace password with empty value
        if (!paramObj.password) delete paramObj.password;

        sails.log.debug(paramObj);

        User.update(req.param('id'), paramObj, function(err) {
            if (err) {
                console.log(err);
                req.session.flash = {
                    err: err
                }
                return res.redirect('/user/edit/' + req.param('id'));
            }
            res.redirect('/user');
        });
    },

    destroy: function(req, res, next) {
        User.findOne(req.param('id'), function(err, user) {
            if (err) return next(err);
            if (!user) return next('User doesn\'t exist.');
            if (!user.id == req.session.id) return next('You can\'t delete yourself');
            User.destroy(req.param('id'), function(err) {
                if (err) return next(err);
            });
            res.redirect('/user');
        });
    },

    transfer: function(req, res, next) {

        let id = req.param('userId');
        let deposit = parseFloat(req.param('ether'));

        if (!id || !deposit) {
            return res.send(400, 'Missing id and/or deposit');
        }

        console.log({
            id: id,
            deposit: deposit
        });
        // TODO call smartcontract
        // then update depositTotal

        User.findOne({
                id: id
            })
            .then(function(user) {
                return User.update(id, {
                    depositTotal: parseFloat(user.depositTotal) + deposit
                });
            })
            .then(function(updatedUser) {
                return res.ok();
            })
            .catch(function(err) {
                sails.log.error(err);
                return res.send(500, err);
            });

    },


    investorList: function(req, res, next) {
        // exclude current user
        let params = req.params.all();
        let start = req.param('start') || 0;
        let limit = req.param('length') || 10;
        let count = 0;
        let conditions = {
            role: User.roles.investor,
            verified: true,
        };
        // handle ordering
        let sort = null;        
        if (params.order && params.order.length > 0) {
            let col = params.columns[params.order[0].column].data;
            let dir = params.order[0].dir.toUpperCase();
            if (col != '' && dir != '') {
                sort = col + ' ' + dir;
            }
        }
        User
            .count(conditions)
            .then(function(res) {
                count = res;
                if (sort) {
                    return User
                        .find(conditions)
                        .sort(sort)
                        .limit(limit)
                        .skip(start);
                } else {
                    return User
                        .find(conditions)
                        .sort('role ASC')
                        .sort('email ASC')
                        .limit(limit)
                        .skip(start);
                }
            })
            .then(function(users) {
                // flatten deposit wallets
                for (let i = 0; i < users.length; i++) {
                    let deposits = users[i].deposits;
                    // wrap wallet with links too
                    // TODO: how can we move it to view?
                    users[i].wallet = `<a href="${sails.config.explorer.ether.replace('%s', users[i].wallet)}" target="_blank">0x${users[i].wallet}</a>`;
                    for (let currency in deposits) {
                        let address = deposits[currency].wallet;
                        let url = sails.config.explorer[currency].replace('%s', address);
                        let balance = deposits[currency].balance;
                        // TODO: how can we move it to view?
                        let link = '<a href="' + url + '" target="_blank">' + address + '</a>';
                        let span = '<span>Balance: ' + balance + '</span>';

                        let balanceBtn = '<button title="Update Balance" class="btn btn-info btn-update" data-investor-id="' + users[i].id + '" data-currency="' + currency + '"><i class="material-icons">cached</i></button>';

                        let forwardBtn = '<button title="Forward funds to beneficiary" class="btn btn-warning btn-forward" data-investor-id="' + users[i].id + '" data-currency="' + currency + '"><i class="material-icons">attach_money</i></button>';
                        users[i][currency] = link + ' ' + span + ' ' + balanceBtn;
                        // display button only if balance available
                        if (balance && balance > 0) {
                            users[i][currency] += ' ' + forwardBtn;
                        }
                    }    
                }
                return res.json({
                    draw: req.param('draw'),
                    data: users,
                    recordsFiltered: count,
                    recordsTotal: count,
                });
            })
            .catch(function(err) {
                sails.log.error(err);
                if (err) return next(err);
            });
    },


    investors: function(req, res) {
        return res.view();
    },


    forwardFunds: function(req, res) {
        let params = req.params.all();
        if (!params.investorId) return res.send(400, 'Invalid investor id: ' + params.investorId);
        if (!params.currency) return res.send(400, 'Invalid currency id: ' + params.currency);

        let deposit = null;
        let to = null;
        let user = null;
        User
            .findOne({ id: params.investorId, role: User.roles.investor })
            .then(function (investor) {
                user = investor;
                deposit = investor.deposits[params.currency];
                to = sails.config.beneficiary[params.currency];                
                if (!deposit) return res.send(400, 'Invalid currency (' + params.currency + ') or no deposit wallet for it');
                if (!to) return res.send(400, 'Invalid currency (' + params.currency + ') or no configured beneficiary for it');
                if (!deposit.balance || deposit.balance <= 0) return res.send(400, 'No balance to forward');
           
                // if not confirmed, return transaction preview
                if (!params.confirmed) return res.json({
                    'from': deposit.wallet,
                    'to': to,
                    'currency': params.currency,
                    'amount': deposit.balance
                });

                // once confirmed, forward funds
                sails.log.debug('Forwarding ' + deposit.balance + ' ' + params.currency + ' from ' + deposit.wallet + ' to ' + to);
                return BlockChainService.sendTransaction(
                    params.currency,
                    deposit.balance,
                    to,
                    deposit.wallet,
                    deposit.privateKey
                );
            })
            .then(function(hash) {
                if (!params.confirmed) return Promise.resolve();
                if (hash) {
                    let sentBalance = deposit.balance;
                    sails.log.debug(deposit.balance + ' ' + params.currency + ' from ' + deposit.wallet + ' successfully forwarded to ' + to + ' hash: ' + hash);

                    user.deposits[params.currency].balance = 0;
                    user.save(function(err) {
                        if (err) {
                            sails.log.error(err);
                            return res.send(500, err);
                        }
                        let result = {
                            'hash': hash,
                            'from': deposit.wallet,
                            'currency': params.currency,
                            'to': to,
                            'amount': sentBalance
                        };
                        return res.json(result);
                    });

                } else {
                    sails.log.error('Error forwarding funds: No hash returned');
                }
            })
            .catch(function(err) {
                sails.log.error('Error forwarding funds: ' + err);
                return res.send(500, err.message);
            });        

    },


    updateBalance: function (req, res) {
        let params = req.params.all();
        if (!params.investorId) return res.send(400, 'Invalid investor id: ' + params.investorId);
        if (!params.currency) return res.send(400, 'Invalid currency id: ' + params.currency);

        let investor = {};
        let balance = 0;
        User
            .findOne({ id: params.investorId, role: User.roles.investor })
            .then(function(user) {
                investor = user;
                return BlockChainService.checkBalance(params.currency, investor.deposits[params.currency].wallet);
            })
            .then(function(bal) {
                balance = bal;
                let amounts = {};
                amounts[params.currency] = balance;
                return InvestorService.updateBalance(investor, amounts);
            })
            .then(function(transactions) {
                let change = 0;
                if (transactions.length > 0) {
                    transactions.forEach(function(tx) {
                        change += tx.value;
                    });
                }
                return res.json({
                    email: investor.email,
                    currency: params.currency,
                    balance: balance,
                    change: change
                });
            })
            .catch(function(err) {
                sails.log.error(err);
                return res.send(500, err.message);
            });
    },


    contributeCoins: function (req, res) {
        let params = req.params.all();
        if (!params.investorId) return res.send(400, 'Invalid investor id: ' + params.investorId);

        let investor = {};
        let balance = 0;
        User
            .findOne({ id: params.investorId, role: User.roles.investor })
            .then(function (user) {
                investor = user;
                // find positive transactions only
                // negative transactions are assumed to not affecting investor contributions                
                return Transaction.find({ userId: investor.id, value: { '>': 0 } });
            })
            .then(function(transactions) {                
                
                let totalToken = 0;
                let deposits = {};
                transactions.forEach(function(tx) {
                    if (!deposits[tx.currency]) deposits[tx.currency] = 0;
                    deposits[tx.currency] += tx.value;
                    totalToken += tx.valueToken;
                });

                // if not confirmed, return transaction preview
                if (!params.confirmed) return res.json({
                    email: investor.email,
                    wallet: investor.wallet,
                    deposits: deposits,
                    totalToken: totalToken
                });

                // contribute coins
                SmartContractService
                    .contributeCoins('0x' + investor.wallet, totalToken.toString())
                    .then(function (obj) {
                        return res.json(obj);
                    })
                    .catch(function (err) {
                        sails.log.error(err);
                        return res.send(500, err);
                    });
            })
            .catch(function (err) {
                sails.log.error(err);
                return res.send(500, err.message);
            });
    },    
};