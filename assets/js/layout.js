// layout.js

$(document).ready(function() {

  // smooth scroll
  $('#navbar-main a').smoothScroll({ offset: -60 });

  // copyright year
  $('.auto-update-year').html(new Date().getFullYear());

});
