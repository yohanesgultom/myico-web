/**
 * InvestorService.js
 * Handle investor business processes
 **/

module.exports = {

    /**
     * Update balance if amount is different
     * and create transaction automatically
     * 
     * Example input:
     * amounts = {
     *  bitcoin: 1.0,
     *  ether: 0.5,
     *  litecoin: 0.0,
     *  dash: 0.1,
     *  bitcoinCash: 0.0,
     * }
     * 
     */
    updateBalance: function (user, amounts) {        
        if (user.role != User.roles.investor) return Promise.reject('Not an investor: ' + user.email);
        // update latest amount and create corresponding transaction
        let transactions = [];
        let now = new Date();
        for (let currency in amounts) {
            if (user.deposits[currency]) {
                let amount = amounts[currency];            
                if (user.deposits[currency].balance == amount) continue;

                let delta = amount;
                if (!isNaN(user.deposits[currency].balance)) {
                    delta = amount - user.deposits[currency].balance;
                }
                user.deposits[currency].balance = amount;
                user.deposits[currency].updatedAt = now;
                // ignore if zero (but can be negative)
                if (delta != 0) {
                    transactions.push({
                        userId: user.id,
                        currency: currency,
                        value: delta.toFixed(10),
                    });
                }
            }
        }

        return new Promise(function(resolve, reject) {
            user.save(function (err) {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });            
        })
        .then(function() {
            // create transaction(s) and update investor balance
            if (transactions.length > 0) {
                return Transaction.create(transactions);
            } else {
                // no transaction made
                return Promise.resolve([]);
            }
        });
    },

}
