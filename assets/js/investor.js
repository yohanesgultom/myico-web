// investor.js

function handleDeposit() {
    event.preventDefault();
    let btn = $(this);
    let modal = $('#modal-deposit-topup');
    let currency = btn.data('currency');

    // TODO handle other currencies
    if (currency != 'ether') {
        alert('Currency not supported');
        return false;
    }

    prepareEtherModal(btn, modal);

    // show modal
    modal.modal({
        keyboard: false
    });
}

// modify deposit topup modal
function prepareEtherModal(btn, modal) {
    // change currency label
    $('label[for=amount]', modal).text(btn.data('currency').toUpperCase());

    // bind process button handler
    $('.btn-process', modal).click(function () {
        web3.eth.getAccounts(function (error, accounts) {
            if (error) {
                alert('Failed to get account');
                return false;
            }

            // get first (base) account
            let account = accounts[0];
            let amount = $('input[name=amount]', modal).val() || 0.0;
            let wallet = btn.data('wallet');

            // invoke transaction with wallet extension
            web3.eth.sendTransaction({
                from: account,
                to: wallet,
                value: web3.toWei(amount).toString()
            }, function (error, result) {
                if (error) {
                    alert(error.message.split('\n')[0]);
                    console.error(error.message);
                    return false;
                }

                alert('Transaction is successful');
                console.log(result);
                modal.modal('hide');
            });
        });
    });
}
var test;
$(document).ready(function () {

    // Generate Qr Code
    $('.btn-qr').map(function () {
        new QRCode($(this).next().get(0), {
         text: $(this).data('wallet-text'),
         width: 200,
         height: 200,
         colorDark : "#000000",
         colorLight : "#ffffff",
         correctLevel : QRCode.CorrectLevel.M
     });
    });
    $('.btn-qr').click(function () {
        // $(this).next().slideToggle();

        var title = "<h4>"+$(this).data('title')+"</h4>";
        var barcode = $(this).next().html().toString();
      //   var addr = $(this).data('wallet-text');
        hm_openDialog(title+barcode,'info');
    });

    // init clipboard copier
    new Clipboard('.btn-clipboard').on('success', function (e) {
        hm_openDialog('copied to clipboard','ok');
    });

    // add wallet
    var addWalletForm = $('#add-wallet-form');
    $('.btn-submit', addWalletForm).on('click', function () {
        var btn = $(this);
        var data = serializeForm(addWalletForm);
        var selectedWalletName = $('option:selected', 'select[name=newWallet]').text();
        if (data.investorId && data.newWallet && confirm('Create new ' + selectedWalletName + ' wallet?')) {
            btn.prop('disabled', true);

            // submit valid form
            $.post('/page/addWallet', data, function (res) {
                hm_openDialog(res.currency + ' wallet added', 'ok');
                setTimeout(function () { location.reload(); }, 1000);
            })
            .fail(function (err) {
                hm_openDialog(err.responseText || 'There seems to be a problem with our server. Please try again or contact our support', 'error');
                console.error(err);
                btn.prop('disabled', false);
            });
        }
    });
});

// metamask is using window.onload
$(window).on('load', function() {
    // show deposit button if web3 available
    // and coin currency supported
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
        $('.btn-send-transaction').each(function () {
            let btn = $(this);
            let currency = btn.data('currency');
            if (currency == 'ether') {
                btn.show().on('click', handleDeposit);
            }
        });
    }
});