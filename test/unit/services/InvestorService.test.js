const chai = require('chai'),
    should = chai.should(),
    expect = chai.expect;

describe('InvestorService', function () {

    describe('#updateBalance', function () {

        let investor;

        before(function (done) {
            User.create({
                email: 'updatebalance1@gmail.com',
                wallet: '81b7E08F65Bdf5648606c89998A9CC8164397647',
                password: '81b7E08F65Bdf5648606c89998A9CC8164397647',
                verified: true,
                deposits: {
                    bitcoin: {
                        wallet: 'n4FyuAzjSKgAXgFP7zViarWDtu8TXJCrTZ',
                        privateKey: 'cTFHKfjan9xBmL1SH7eeEo3zJgjRkrL7WhWuR5bVoVTLWjADi6JY',
                    },
                    ether: {
                        wallet: '527ef560fca4f675fb1559a74de8307786fca7a9',
                        privateKey: '7f7ad73b3b06f0494e97615d83c5e2f09fc787836c1c2c798890dee260e41825',
                    },
                    bitcoinCash: {
                        wallet: 'muWmyGWR2z5Nb18m5ui7wrVgc84UsvLBNH',
                        privateKey: 'cPS2Yq3m1Nc6d3JzFy5hZZrp8u67crbD9rHwCcVrYt3uNuGadZLv',
                    },
                    dash: {
                        wallet: 'yP72P9rZYggDWT9tJYUVHFNhbR1GjeDwnF',
                        privateKey: 'cVY3VJ63D4BYnRjh7Pn4xZaLtVbseQ5AD35g48UtL34b3mqtuM3o',
                    },
                    litecoin: {
                        wallet: 'LQZbqytqhrHH8dREXrov9iVg9J3SSMxyuC',
                        privateKey: 'T4hzKKBWu2PPLXfoBaWvjR94F6QXxr3G5SNnQLK2Lsa4zHDBMtr8',
                    }
                }
            })
            .then(function (user) {
                investor = user;
                done();
            })
            .catch(done);
        });

        it('should be able to update positive balance for first time', function (done) {
            let amounts = {
                bitcoin: 1.0,
                ether: 2.0,
                dash: 3.0,
                litecoin: 4.0,
                bitcoinCash: 5.0,
            };

            InvestorService
                .updateBalance(investor, amounts)
                .then(function () {
                    return User.findOne({id: investor.id});
                })
                .then(function (updatedUser) {
                    for (let currency in amounts) {
                        updatedUser.deposits[currency].balance.should.equal(amounts[currency]);
                    }
                    return Transaction.find({userId: investor.id});
                })
                .then(function (transactions) {
                    transactions.length.should.equal(Object.keys(amounts).length);
                    transactions.forEach (function (t) {
                        t.value.should.equal(amounts[t.currency]);
                    });
                    done();
                })
                .catch(done);
        });


        it('should be able to update increasing and decreasing balance and ignore unchanged balance', function (done) {
            let amounts = {
                bitcoin: 1.5, // increasing
                ether: 1.7, // decreasing
                dash: 3.0, // unchanged
                litecoin: 4.0, // unchanged
                bitcoinCash: 5.0, // unchanged
            };

            InvestorService
                .updateBalance(investor, amounts)
                .then(function () {
                    return User.findOne({ id: investor.id });
                })
                .then(function (updatedUser) {
                    for (let currency in amounts) {
                        updatedUser.deposits[currency].balance.should.equal(amounts[currency]);
                    }
                    return Transaction.find({ userId: investor.id });
                })
                .then(function (transactions) {
                    let total = {};
                    // summarize each transaction per wallet
                    transactions.forEach(function (t) {
                        if (!total[t.currency]) total[t.currency] = 0.0;
                        total[t.currency] += t.value;
                    });
                    // summary should equals latest amount
                    for (let currency in amounts) {
                        total[currency].should.equal(amounts[currency]);
                    }
                    done();
                })
                .catch(done);
        });


        it('should handle no change at all by returning empty transaction', function (done) {
            let amounts = {
                bitcoin: 1.5, 
                ether: 1.7,
                dash: 3.0,
                litecoin: 4.0,
                bitcoinCash: 5.0,
            };

            InvestorService
                .updateBalance(investor, amounts)
                .then(function (transactions) {
                    // no transaction made
                    transactions.length.should.equal(0);
                    return User.findOne({ id: investor.id });
                })
                .then(function (updatedUser) {
                    // balance unchanged
                    for (let currency in amounts) {
                        updatedUser.deposits[currency].balance.should.equal(amounts[currency]);
                    }
                    done();
                })
                .catch(done);
        });


    });


});
