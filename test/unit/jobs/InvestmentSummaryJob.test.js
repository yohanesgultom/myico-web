const chai = require('chai'),
    nock = require('nock'),
    should = chai.should(),
    expect = chai.expect

const job = require('../../../api/jobs/InvestmentSummaryJob')({});

describe('InvestmentSummaryJob', function () {

    before(function (done) {
        // delete existing investors, transactions, params and cache
        User
            .destroy({role: User.roles.investor})
            .then(function() {
                return Transaction.destroy({value: {not: null} })
             })
            .then(function() {
                return Cache.destroy({ key: { not: null } })
            })
            .then(function() {
                done();
            })
            .catch(done);
    })

    describe('#run implicitly by adding transactions', function () {

        let dummyInvestor = {
            email: 'investment.summary@gmail.com',
            wallet: 'XXb865311908081b743b8d3ff075DF40c12aeBdF',
            password: 'XXb865311908081b743b8d3ff075DF40c12aeBdF',
            verified: true,
            deposits: {
                bitcoin: {
                    wallet: 'n4FyuAzjSKgAXgFP7zViarWDtu8TXJCrTZ',
                    privateKey: 'cTFHKfjan9xBmL1SH7eeEo3zJgjRkrL7WhWuR5bVoVTLWjADi6JY',
                },
                ether: {
                    wallet: '527ef560fca4f675fb1559a74de8307786fca7a9',
                    privateKey: '7f7ad73b3b06f0494e97615d83c5e2f09fc787836c1c2c798890dee260e41825',
                },
                bitcoinCash: {
                    wallet: 'muWmyGWR2z5Nb18m5ui7wrVgc84UsvLBNH',
                    privateKey: 'cPS2Yq3m1Nc6d3JzFy5hZZrp8u67crbD9rHwCcVrYt3uNuGadZLv',
                },
                dash: {
                    wallet: 'yP72P9rZYggDWT9tJYUVHFNhbR1GjeDwnF',
                    privateKey: 'cVY3VJ63D4BYnRjh7Pn4xZaLtVbseQ5AD35g48UtL34b3mqtuM3o',
                },
                litecoin: {
                    wallet: 'LQZbqytqhrHH8dREXrov9iVg9J3SSMxyuC',
                    privateKey: 'T4hzKKBWu2PPLXfoBaWvjR94F6QXxr3G5SNnQLK2Lsa4zHDBMtr8',
                }
            }
        };

        before(function (done) {
            // create investor
            User.create(dummyInvestor)
                .then(function(user) {
                    investor = user;
                    return Transaction.create([
                        {userId: investor.id, currency: 'bitcoin', value: 1.0},
                        {userId: investor.id, currency: 'bitcoin', value: -1.0},
                        {userId: investor.id, currency: 'ether', value: 2.0},
                        {userId: investor.id, currency: 'ether', value: -2.0},
                    ])
                })
                .then(function(transactions) {
                    done();
                })
                .catch(done);
            
        });

        it('should only summarize positive transactions', function (done) {
            this.timeout(5000);
            let cacheKey = 'investmentSummary';
            // wait until investmentSummaryJobs done
            setTimeout(function() {
                Cache
                    .findOne({ key: cacheKey })
                    .then(function (cache) {
                        cache.value.totalUSD.should.equal(6298.95380744712);
                        cache.value.totalToken.should.equal(63928.08219178083);
                        done();
                    });
            }, 1000);
        });

    });


});
