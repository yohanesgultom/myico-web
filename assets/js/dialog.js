function hm_prepareDialog() {
  var dlg = $('.hm-dialog-layer');
  if (dlg.length <= 0) {
    dlg = $('<div class="hm-dialog-layer"></div><div class="anim hm-dialog hm-block-item hm-big-box dialog-ok"><div class="hm-dialog-bg"></div><div class="hm-dialog-icon"></div><div class="hm-dialog-body"></div><div class="hm-dialog-footer"><div class="text-left"><a href="#" class="btn hm-btn-green pull-right" onclick="hm_closeDialog();return false;">OK</a></div></div></div>');
    $('body').append(dlg);
  }
}

function hm_openDialog(msg,type) {
  var dlg = $('.hm-dialog-layer');
  if (!type) type = 'ok';
  if (dlg.length <= 0) {
    dlg = $('<div class="hm-dialog-layer"></div><div class="anim hm-dialog hm-block-item hm-big-box dialog-'+type+'"><div class="hm-dialog-bg"></div><div class="hm-dialog-icon"></div><div class="hm-dialog-body">' + msg + '</div><div class="hm-dialog-footer"><div class="text-left"><a href="#" class="btn hm-btn-green pull-right" onclick="hm_closeDialog();return false;">OK</a></div></div></div>');
    $('body').append(dlg);
  } else {
    $('.hm-dialog').attr('class','anim hm-dialog hm-block-item hm-big-box dialog-'+type);
    $('.hm-dialog .hm-dialog-body').html(msg);
  }
  $('body').addClass('with-dialog');
}

function hm_closeDialog() {
  $('.hm-dialog .hm-dialog-body').html('');
  $('body').removeClass('with-dialog');
}