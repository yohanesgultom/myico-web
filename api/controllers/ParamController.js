/**
 * ParamController
 *
 * @description :: Server-side logic for managing Params
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    'new': function(req, res) {
        res.view();
    },

    index: function(req, res) {
        res.view();
    },

    create: function(req, res) {

        var paramObj = {
            key: req.param('key'),
            value: req.param('value'),
        }

        Param.create(paramObj, function(err, param) {
            if (err) {
                console.log(err);
                req.session.flash = {
                    err: err
                };
                return res.redirect('/param/new');
            }
            res.redirect('/param');
        });
    },

    list: function(req, res, next) {
        let start = req.param('start') || 0;
        let limit = req.param('length') || 10;
        let count = 0;
        Param
            .count()
            .then(function(res) {
                count = res;
                return Param
                    .find()
                    .sort('key ASC')
                    .limit(limit)
                    .skip(start);
            })
            .then(function(params) {
                return res.json({
                    draw: req.param('draw'),
                    data: params,
                    recordsFiltered: count,
                    recordsTotal: count,
                });
            })
            .catch(function(err) {
                sails.log.error(err);
                if (err) return next(err);
            });
    },


    edit: function(req, res, next) {

        Param.findOne(req.param('id'), function(err, param) {
            if (err) return next(err);
            if (!param) return next('param doesn\'t exist.');
            res.view({
                param: param
            });
        });
    },

    update: function(req, res, next) {

        var paramObj = {
            key: req.param('key'),
            value: req.param('value'),
        }

        Param.update(req.param('id'), paramObj, function(err) {
            if (err) {
                console.log(err);
                req.session.flash = {
                    err: err
                }
                return res.redirect('/param/edit/' + req.param('id'));
            }
            res.redirect('/param');
        });
    },

    destroy: function(req, res, next) {
        Param.findOne(req.param('id'), function(err, param) {
            if (err) return next(err);
            if (!param) return next('Param doesn\'t exist.');
            Param.destroy(req.param('id'), function(err) {
                if (err) return next(err);
            });
            res.redirect('/param');
        });
    },

};