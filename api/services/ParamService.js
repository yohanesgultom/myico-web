/**
 * ParamService.js
 * Get parameter stored in database
 **/

const moment = require('moment');

/**
 * Get nearest endDateTime among phases
 * Example of valid phases param:
 * 
    preSale: {
      name: 'Pre-Sale',
      priceETH: 0.00013333333333333334,
      endDateTime: '2017-11-30 23:59:59.000+07:00',
    },
    sale: {
      name: 'Sale',
      priceETH: 0.0003333333333333333,
      endDateTime: '2017-12-31 23:59:59.000+07:00',
    }

 * @param {*} param
 * @param {*} date 
 */
const getNextPhase = function (phases, momentDate) {  
  let phase, tempPhase;
  if (phases) {
    momentDate = momentDate || moment();
    for (let key in phases) {
      tempPhase = phases[key];
      if ((!phase || moment(tempPhase.endDateTime).isBefore(moment(phase.endDateTime))) 
        && moment(tempPhase.endDateTime).isAfter(momentDate)) {
        phase = tempPhase;
      }
    }
  }
  return phase;
}

module.exports = {

  getNextPhase: getNextPhase,

  getAcceptedCurrenciesAndCurentPhase: function (date) {
    date = date || moment();
    let currencies, phase;
    let tempMoment = null;
    return Param
      .find({ key: ['acceptedCurrencies', 'phases'] })
      .then(function (params) {        
        params.forEach(function (param) {
          if (param.key == 'acceptedCurrencies') {
            currencies = param.value;
          } else if (param.key == 'phases') {
            if (param.value) {
              // get closest end date time from today
              phase = getNextPhase(param.value);
            }
          }
        });
        return {
          acceptedCurrencies: currencies,
          currentPhase: phase,
        };
      });
  },

  /**
   * Get multiple param by keys
   * returned as a map
   */
  getMultipleParams: function (keys) {
    let result = {};
    return Param
      .find({ key: keys })
      .then(function (params) {
        params.forEach(function (param) {
          result[param.key] = param.value;
        });
        return result;
      });
  },

  updateCurrenciesExchangeRate: function () {
    let coins = [];
    let acceptedCurrencies, currentPhase;
    let now = new Date();
    sails.log.debug('Updating currencies exchange rate..');
    return ParamService
      .getAcceptedCurrenciesAndCurentPhase()
      .then(function (param) {
        if (!param || !param.acceptedCurrencies || !param.currentPhase) {
          throw new Error('param(s) not found');
        }
        acceptedCurrencies = param.acceptedCurrencies;
        currentPhase = param.currentPhase;
        // get currencies symbols
        for (let key in acceptedCurrencies) {
          coins.push(acceptedCurrencies[key].symbol);
        }
        // get USD to coins
        return BlockChainService.getCurrentExchangeRate('USD', coins);
      })
      .then(function (ratePerUSD) {
        sails.log.debug(ratePerUSD);
        // calculate & update token price
        sails.log.debug(currentPhase);
        for (let key in acceptedCurrencies) {
          let symbol = acceptedCurrencies[key].symbol;
          acceptedCurrencies[key].ratePerToken = currentPhase.priceETH / ratePerUSD['ETH'] * ratePerUSD[symbol];
          acceptedCurrencies[key].rateToETH = ratePerUSD['ETH'] / ratePerUSD[symbol];
          acceptedCurrencies[key].rateToUSD = 1 / ratePerUSD[symbol];
          acceptedCurrencies[key].lastUpdatedAt = now;
        }
        // update database
        sails.log.debug(acceptedCurrencies);
        return Param.update({ key: 'acceptedCurrencies' }, {
          key: 'acceptedCurrencies',
          value: acceptedCurrencies
        });
      });
  }
}
