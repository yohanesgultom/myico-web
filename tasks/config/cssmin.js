/**
 * Compress CSS files.
 *
 * ---------------------------------------------------------------
 *
 * Minify the intermediate concatenated CSS stylesheet which was
 * prepared by the `concat` task at `.tmp/public/concat/production.css`.
 *
 * Together with the `concat` task, this is the final step that minifies
 * all CSS files from `assets/styles/` (and potentially your LESS importer
 * file from `assets/styles/importer.less`)
 *
 * For usage docs see:
 *   https://github.com/gruntjs/grunt-contrib-cssmin
 *
 */
module.exports = function(grunt) {

  grunt.config.set('cssmin', {
    dist: {
      files: {
        '.tmp/public/vendors/widget.min.css': ['.tmp/public/vendors/widget.css'],
        '.tmp/public/vendors/propeller.min.css': ['.tmp/public/vendors/propeller.css'],
        '.tmp/public/css/styles.min.css': ['.tmp/public/css/styles.css'],
      },            
    }
  });

  grunt.loadNpmTasks('grunt-contrib-cssmin');
};
