module.exports = function(agenda) {
    var job = {

        // execute job
        run: function(job, done) {
            sails.log.debug(job.attrs.name + ' is running..');            
            let limit = job.attrs.data && job.attrs.data.limit ? job.attrs.data.limit : 1;
            User
                .find({ role: User.roles.investor, verified: true })
                .sort('updatedAt ASC')
                .limit(limit)
                .then(function(investors) {
                    let updateBalancePromises = [];
                    investors.forEach(function (investor) {
                        sails.log.debug('Updating wallets balance: ' + investor.email + ' ..');
                        let checkBalancePromises = [];
                        for (let currency in investor.deposits) {                            
                            checkBalancePromises.push(BlockChainService
                                .checkBalance(currency, investor.deposits[currency].wallet)
                                .then(function (amountStr) {
                                    sails.log.debug(investor.email + ' ' + investor.deposits[currency].wallet + ' (' + currency + ') = ' + amountStr);
                                    return {
                                        currency: currency,
                                        amount: parseFloat(amountStr)
                                    };
                                })
                                .catch(function (err) {
                                    sails.log.error(err);
                                    return {
                                        currency: currency,
                                        amount: 0.0,
                                    }
                                })
                            );
                        }

                        let updateBalancePromise = Promise
                            .all(checkBalancePromises)
                            .then(function(amountList) {
                                let amounts = {};
                                amountList.forEach(function (a) {
                                    amounts[a.currency] = a.amount;
                                });
                                return InvestorService.updateBalance(investor, amounts);
                            })
                            .then(function(transactions) {
                                if (transactions.length <= 0) {
                                    return null;
                                } else {
                                    return {
                                        user: investor,
                                        transactions: transactions,
                                    }
                                }
                            });
                        
                        // pool all update promises
                        updateBalancePromises.push(updateBalancePromise);
                    });

                    return Promise.all(updateBalancePromises);
                })
                .then(function (newTransactions) {
                    newTransactions = newTransactions.filter(function (x) { return x });
                    sails.log.debug(newTransactions);
                    if (newTransactions.length > 0) {
                        // send notification email
                        let baseUrl = AppService.getBaseUrl();
                        Jobs.now('SendMailJob', {
                            view: 'newTransactionEmail',
                            locals: {
                                layout: '../layout',
                                newTransactions: newTransactions,
                            },
                            options: {
                                to: sails.config.email.from,
                                subject: 'New transaction(s)',
                            }
                        });
                    }

                    // done
                    sails.log.debug(job.attrs.name + ' is completed.');
                    done();
                })
                .catch(function(err) {
                    sails.log.error(job.attrs.name + ' hit error.');
                    sails.log.error(err.message);
                    done(err);
                });
            
        },
    };
    return job;
}