module.exports = function(agenda) {
    var job = {

        // execute job
        run: function(job, done) {
            sails.log.debug(job.attrs.name + ' is running..');
            ParamService
                .updateCurrenciesExchangeRate()
                .then(function(res) {
                    sails.log.debug(job.attrs.name + ' is completed.');
                    done();
                })
                .catch(function(err) {
                    sails.log.error(job.attrs.name + ' hit error.');
                    sails.log.error(err.message);
                    done(err);
                });
        },
    };
    return job;
}