const chai = require('chai'),
  chaiHttp = require('chai-http'),
  should = chai.should(),
  expect = chai.expect,
  fs = require('fs'),
  path = require('path');

chai.use(chaiHttp);


describe('PageController', function () {

  describe('#addWallet', function () {

    let testUser = {
      email: 'test5@email.com',
      wallet: '893BA6a4Edd7209d3CC865e1a74D3b54ec657046',
      password: '893BA6a4Edd7209d3CC865e1a74D3b54ec657046',
      deposits: {},
    }

    before(function (done) {
      // create test user
      User
        .create(testUser)
        .then(function (user) {
          testUser = user;
          done();
        })
        .catch(done);
    });


    it('should be able to add Bitcoin wallet to existing investor', function (done) {
      // confirm there is no Bitcoin 
      expect(testUser.deposits.bitcoin).to.equal(undefined);
      // add wallet
      chai
        .request(sails.hooks.http.app)
        .post('/page/addWallet')
        .send({
          investorId: testUser.id,
          newWallet: 'bitcoin',
        })
        .end(function (err, res) {
          res.status.should.equals(200);
          User
            .findOne(testUser.id)
            .then(function (user) {
              user.deposits.bitcoin.wallet.length.should.be.above(16);
              user.deposits.bitcoin.privateKey.length.should.be.above(1);
              done();
            })
            .catch(done);
        });
    });
  });

});