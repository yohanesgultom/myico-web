/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

/**
 * Start jobs and trigger callback
 * @param {*} cb 
 */
function startJobs(cb) {
  if (!sails.config.jobs.disabled) {
    // delete existing jobs to prevent redundancy
    Jobs.cancel({type: 'normal'}, function(err, numRemoved) {
      // Jobs.now('InvestmentSummaryJob');
      // Jobs.every('15 minutes', 'ExchangeRateJob');
      // Jobs.every('5 minutes', 'UpdateBalanceJob');
    });
  }
  return cb();
}

module.exports.bootstrap = function(cb) {

  if (sails.config.models.migrate != 'drop') {
    return startJobs(cb);
  }

  // website initial data

  let promises = [];

  // administrator
  promises.push(User.create({
    email: 'admin@myico.ga',
    password: 'admin',
    verified: true,
    role: User.roles.admin
  }));


  // ICO phases param
  promises.push(
    Param.create({
      key: 'phases',
      value: {
        preparation: {
          name: 'Pre-Sale',
          priceETH: 0.00013333333333333334,
          endDateTime: '2017-09-30 23:59:59.000+07:00',
        },
        sale: {
          name: 'Sale',
          priceETH: 0.0003333333333333333,
          endDateTime: '2017-12-31 23:59:59.000+07:00',
        }
      }
    })
  );

  // social media param
  promises.push(
    Param.create({
      key: 'socialMedia',
      value: {
        "email": "support@myico.ga",
        "facebook": "myico",
        "twitter": "@myico",
      }
    })
  );

  // accepted crypto currencies param
  promises.push(
    Param.create({
      key: 'acceptedCurrencies',
      value: {
        bitcoin: {
          name: 'Bitcoin',
          symbol: 'BTC',
          img: '/images/crypto/bitcoin.png',
          ratePerToken: 0.00001726278451078924,
          rateToETH: 19.309360730593607,
          rateToUSD: 5707.762557077625,
        },
        ether: {
          name: 'Ether',
          symbol: 'ETH',
          img: '/images/crypto/ether.png',
          ratePerToken: 0.0003333333333333333,
          rateToETH: 1,
          rateToUSD: 295.59562518474723,
        },
      }
    })
  );

  // initialize summary
  promises.push(
    Cache.create({
      key: 'investmentSummary',
      value: {
        totalUSD: 0.0,
        totalToken: 0.0,
        distributions: {},
        count: 0,
      }
    })
  );

  if (process.env.NODE_ENV != 'seed') {
    return Promise
      .all(promises)
      .then(function () {
        sails.log.debug('Bootstrap completed.')
        startJobs(cb);
      });
  }

  // dummy data for demo

  // generating wallets takes time
  // so we only do it on NODE_ENG=seed
  const faker = require('faker');

  let num = 2;
  let users = [];
  console.log('Generating ' + num + ' dummy investor(s)...');
  for (let i = 0; i < num; i++) {
    let email = faker.internet.email();
    let wallet = BlockChainService.generateWallet('ether');
    let bitcoinDeposit = BlockChainService.generateWallet('bitcoin');

    users
      .push({
          email: email,
          wallet: wallet.address,
          password: wallet.address,
          verified: true,
          deposits: {
            bitcoin: {
              wallet: bitcoinDeposit.address,
              privateKey: bitcoinDeposit.privateKey
            },
            ether: {
              wallet: etherDeposit.address,
              privateKey: etherDeposit.privateKey
            },
          }
        });
  }

  // execute all promises
  Promise
    .all(promises)
    .then(function () {
      return ParamService.updateCurrenciesExchangeRate()
    })
    .then(function () {
      return User.create(users);
    })
    .then(function (users) {
      sails.log.debug('Generating dummy transactions..');
      let trans = [];
      users.forEach(function (user) {
        let n = faker.random.number({ min: 1, max: 5 });
        let currency = faker.random.arrayElement([
          'bitcoin', 'ether'
        ]);
        for (let i = 0; i < n; i++) {
          trans.push({
            userId: user.id,
            currency: currency,
            value: faker.finance.amount(0, 10, 4)
          });
        }
      });
      return Transaction.create(trans);
    })
    .then(function () {
      sails.log.debug('Bootstrap completed.')
      startJobs(cb);
    });
};
