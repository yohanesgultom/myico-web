var sails = require('sails');

before(function(done) {

  // Increase the Mocha timeout so that Sails has enough time to lift.
  this.timeout(5000);

  sails.lift({
    // configuration for testing purposes
    models: {
      connection: 'localDiskDb',
      migrate: 'drop',
    },
    email: {
      from: 'test@holdme.io',
      testMode: true,
    },
    log: {
      level: 'silent',
    },
  }, function(err) {
    if (err) return done(err);
    // here you can load fixtures, etc.
    done(err, sails);
  });
});

after(function(done) {
  // here you can clear fixtures, etc.
  sails.lower(done);
});
