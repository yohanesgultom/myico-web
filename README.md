# MyICO Web

ICO website built with [Sails](http://sailsjs.org) application. This website is integrated with [Holdme Company Token](github.com/holdmeio/holdme-company-token)

## Prerequisites

For running:

* Node.js >= 6.11.0
* Mongodb >= 3.4.9 (required for website data & jobs queue)

For development:

* Sails.js `npm i sails -g`
* Nodemon (optional) `npm i nodemon -g`

## Setup

Development setup steps:

1. Create database in `mongo`
1. Copy `config/local.js.example` and into `config/local.js` and adjust the configuration esp. `models` and `jobs`
1. Install dependencies `npm install`
1. Copy smartcontract (eg. https://github.com/holdmeio/holdme-company-token) build files (`build/contracts`) to `contracts`
1. Run webapp `sails lift` or `nodemon` (to disable server restart on assets change use `nodemon --ignore assets/`). By default, it will be accessible on `http://localhost:1337`

## Testing

Run test with `npm test` (which will trigger `mocha`)

## Development guide

### Assets compilation

Sails.js use grunt to compile assets. These are how-to for common use cases:

* Automatically minify css: add entry in `tasks/config/cssmin.js`
* Automatically minify/uglify js: add entry in `tasks/config/uglify.js`

### Batch jobs

We use a sails' hook [sails-hook-jobs](https://github.com/vbuzzano/sails-hook-jobs) to manage the jobs. It will create a collection in MongoDB database to maintain the jobs queue. To add jobs, create a `*Job.js` file under `api/jobs` and start it in `config/bootstrap.js` (for scheduled jobs) or call it from controller/service (for one-time batch job). For further details, check the documentation on `sails-hook-jobs` readme.

### Automated tests

Add integration (controllers) tests and unit tests (services, models) in `test/integration` and `test/unit` respectively. Make sure each test files use `.test.js` extension for them to be automatically executed on `npm test`.

## Production guide

### Environment configuration

We should set `sails.config.environment=production` globally by putting `export NODE_ENV=production` in server startup (eg. `~/.bashrc` in Ubuntu/Debian) and running `source ~/.bashrc` to apply it. We also need to remove `config/local.js` and create similar configuration in `config/env/production.js` containing specific production configuration. Make sure to do all these things before setting up node process manager such as `pm2`

### Process manager

Install `pm2` globally by running `npm i -g pm2` and setup process management by running:

1. Setup managed process: enter project dir and run `pm2 start app.js --name holdme-web`
1. Enable automatic startup in case of server restart: `pm2 startup` (then copy & paste and run generated command)
1. Save list of managed process: `pm2 save`

### Session store

As sugested [here](http://sailsjs.com/documentation/concepts/deployment/faq#?whats-this-warning-about-the-connect-session-memory-store), we should not use local session store in server especially production due to performance issue. Config example `config/local.js.example` has already used MongoDB for session adapter (leveraging `connect-mongo` package)
