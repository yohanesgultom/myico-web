
$(document).ready(function () {

    let d = $('#data-table-params').DataTable({
        'searching': false,
        'lengthChange': false,
        'ordering': false,
        'processing': true,
        'serverSide': true,
        'ajax': '/param/list',
        'columnDefs': [{
            'searchable': false,
            'orderable': false,
            'targets': 0
        }],
        'columns': [
            { 
                'data': null,
                'render': function (data, type, full, meta) {
                    return d.page.info().start + meta.row + 1;
                }
            },
            { 'data': 'id', 'visible': false },
            { 'data': 'key' },
            { 'data': 'value' },
            { 
                'data': null, 
                'render': function (data, type, full, meta) {
                    return '<a href="/param/edit/' + full.id + '" class="btn btn-primary btn-sm" role="button"><i class="material-icons">create</i></a>' +
                    '<a href="/param/destroy/' + full.id + '" class="btn btn-danger btn-sm" onclick="return confirm(\'Delete this record?\')" role="button"><i class="material-icons">clear</i></a>';
                }
            },
        ]
    });

});
