setInterval(function(){
   var $countdown = $('#countdown_dashboard');
   var deadline = $countdown.data('deadline');
   if (deadline) {
     var _countdown_date = moment.utc(deadline);
     var different = getTimeRemaining(_countdown_date);
     $('.days_dash').children().get(0).innerHTML = different.days[0];
     $('.days_dash').children().get(1).innerHTML = different.days[1];
     $('.hours_dash').children().get(0).innerHTML = different.hours[0];
     $('.hours_dash').children().get(1).innerHTML = different.hours[1];
     $('.minutes_dash').children().get(0).innerHTML = different.minutes[0];
     $('.minutes_dash').children().get(1).innerHTML = different.minutes[1];
     $('.seconds_dash').children().get(0).innerHTML = different.seconds[0];
     $('.seconds_dash').children().get(1).innerHTML = different.seconds[1];
   }
},1000);


function getTimeRemaining(endtime){
   var t = Date.parse(endtime) - Date.parse(moment.utc());
   var seconds = Math.floor( (t/1000) % 60 );
   var minutes = Math.floor( (t/1000/60) % 60 );
   var hours = Math.floor( (t/(1000*60*60)) % 24 );
   var days = Math.floor( t/(1000*60*60*24) );
   return {
     'total': t,
     'days': days.toString().padStart(2,'0').split(''),
     'hours': hours.toString().padStart(2,'0').split(''),
     'minutes': minutes.toString().padStart(2,'0').split(''),
     'seconds': seconds.toString().padStart(2,'0').split('')
   };
 }