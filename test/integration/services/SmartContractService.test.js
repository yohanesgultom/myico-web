const chai = require('chai'),
    moment = require('moment'),
    nock = require('nock'),
    should = chai.should(),
    expect = chai.expect;

describe('SmartContractService', function () {

    describe('#totalTokenIssued', function () {

        it('should return totalTokenIssued', function (done) {
            this.timeout(20000);
            SmartContractService
                .totalTokenIssued()
                .then(function(totalToken) {
                    sails.log.debug('totalToken=' + totalToken);
                    totalToken.length.should.above(0);
                    done();
                })
                .catch(done);            
        });

    });


    describe('#getWhitelist', function () {

        it('should return whitelist', function (done) {
            let address = SmartContractService.getWhitelist();
            sails.log.debug('whitelist=' + address);
            address.length.should.above(0);
            done();
        });

    });


    describe('#getWhitelists', function () {

        it('should return whitelisted addresses', function (done) {
            this.timeout(20000);
            SmartContractService
                .getWhitelists()
                .then(function (whitelists) {
                    sails.log.debug('whitelists=' + whitelists);
                    whitelists.length.should.above(0);
                    done();
                })
                .catch(done);
        });

    });


    describe('#getWhitelistBalance', function () {

        it('should return whitelist balance', function (done) {
            this.timeout(20000);
            SmartContractService
                .getWhitelistBalance()
                .then(function (balance) {
                    sails.log.debug('whitelist balance=' + balance);
                    balance.length.should.above(0);
                    done();
                })
                .catch(done);
        });

    });

    // TODO: too immutable
    // describe('#contributeCoins', function () {

    //     it('should be able to contribute coins', function (done) {
    //         this.timeout(120000);
    //         SmartContractService
    //             .contributeCoins('0x46467CA9FfE3E8A5eB9BfB8f50797A324BB93573', 0.1)
    //             .then(function (res) {
    //                 sails.log.debug('contributeCoins response=' + res);
    //                 expect(res).should.not.be.null;
    //                 res.tx.length.should.above(0);
    //                 done();
    //             })
    //             .catch(done);
    //     });

    // });

});
